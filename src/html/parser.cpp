#include "astNode.cpp"

#include <stack>
#include <map>
#include <fstream>
#include <string>
#include "../lib/codefrag.cpp"
using namespace std;

#define PARSER_GUARD 1

namespace htmlAst{



    enum class wordType{
        lPar, lParS,              // <  </  
        rPar, rParS,              // >  />   
        word, string,
        equals, space, enter

    };


    class tag{
        public:
        string name;
        bool lSlash, rSlash;
        map<string,string> args;
        codefrag text;
        bool plainText;
    };



        /// @brief Loads to string contents of the given file  
    bool load(string filename, string& out){

        string s;
        ifstream in;
        in.open(filename);
        if(!in.good()) return 0;

        while(!in.eof()){
            getline(in,s);
            out+=s+"\n";
        }
        return 1;

    }



        /// @brief Returns true iff dest is infix of src at index start
    bool pref(string& src, int start, const string dest){
        if(start+dest.size() >= src.size()) return 0;
        return src.substr(start, dest.size()) == dest;
    }


   

        /// @brief Turns string into lexeme
    bool lexer(string raw, vector<pair<string, wordType>>& out){

        #define constToken(tok, size, name)         \
            if(pref(raw, i, tok)){                  \
                out.pb({tok, wordType::name});      \
                i+=size;                            \
                continue;                           \
            }


        int i = 0;
        while(i<raw.size()){

                // comment
            if(pref(raw, i, "<!--")){
                while(i<raw.size() && !pref(raw, i, "-->")) i++;
                if(i == raw.size()) return 0;
                i+=3;
                continue;
            }

                // <! DOCTYPE>
            if(pref(raw, i, "<!")){
                while(i<raw.size() && !pref(raw, i, ">")) i++;
                if(i == raw.size()) return 0;
                i+=1;
                continue;
            }
                //ignore java (it deserves it)
            if(pref(raw, i, "<script")){
                while(i<raw.size() && !pref(raw, i, "</script>")) i++;
                if(i == raw.size()) return 0;
                i+=9;
                continue;
            }
                //ignore css 
            if(pref(raw, i, "<style")){
                while(i<raw.size() && !pref(raw, i, "</style>")) i++;
                if(i == raw.size()) return 0;
                i+=8;
                continue;
            }

            constToken("</", 2, lParS);
            constToken("<", 1, lPar);
            constToken("/>", 2, rParS);
            constToken(">", 1, rPar);
            constToken("=", 1, equals);
            constToken(" ", 1, space);
            constToken("\t", 1, space);
            constToken("\n", 1, enter);

                // string
            if(raw[i] == '\"' || raw[i]=='\''){
                bool strongString = raw[i] == '\"';
                string text;
                i++;
                while(i<raw.size() && (raw[i]!='\"' && (raw[i]!='\'' || strongString)))     
                    text += raw[i++];
                if(i == raw.size()) return 0;
                i++;
                out.pb({text, wordType::string});
                continue;
            }

                //word
            string text = "";
            text += raw[i++];
            while(i<raw.size() && ( raw[i]!='>' && raw[i]!='/'  && raw[i]!='=' && raw[i]!='<' && !isspace(raw[i]) ))  
                text += raw[i++];
            out.pb({text, wordType::word});

        }

        return 1;

    }


   


    map<string,string> amp = {
        {"lt", "<"},  {"gt", ">"}, {"amp", "&"}, {"#8211", "-"}, {"#8212", "--"}, {"nbsp", " "},
        {"quot", "\""}, {"#34", "\""}, {"#8220", "\""}, {"#8221", "\""}, {"#8222", "\""},
        {"#39", "\'"}, {"#8216", "\'"},  {"#8217", "\'"}, {"#8218", ","}
    };


    /// @brief Replaces instances of &...; into corresponding strings
    void ampResolution(codefrag&c){
        codefrag res;

        for(auto x:c){
            string r;
            if(x.isSpec()){
                res << x;
                continue;
            }
            for(int i=0;i<x.size();i++){
                if(x[i]=='&'){
                    string w;
                    i++;
                    while(x[i]!=';') w+=x[i], i++;
                    if(amp[w]=="") r+=w;
                    r+=amp[w];
                }
                else r+=x[i];
            }
            res << r;
        }

        c = res;
    }



    

    /// @brief Builds sequence of tags
    bool parser(vector<pair<string, wordType>> raw, vector<tag>&res){

        #define skipWhite() \
            while(raw[i].nd==wordType::space || raw[i].nd==wordType::enter) \
                i++;

        #define RETURN(a) \
            {cout<<a<<endl; return 0;}


        int i = 0;

        while(i<raw.size()){

                //Tag
            if(raw[i].nd==wordType::lPar || raw[i].nd==wordType::lParS){
                tag t;
                t.lSlash =  raw[i].nd==wordType::lParS;
                i++;
                skipWhite();

                if(raw[i].nd != wordType::word) RETURN("Expected name of the tag");
                t.name = raw[i].st;
                i++;
                skipWhite();

                    //Arg
                while(raw[i].nd == wordType::word){
                    string argName = raw[i].st;
                    i++;
                    skipWhite();
                    if(raw[i].nd==wordType::rPar || raw[i].nd==wordType::rParS) break;
                    //if(raw[i].nd != wordType::equals) RETURN("Expected '=', was: " + raw[i].st);
                    i++;
                    skipWhite();
                    if(raw[i].nd != wordType::string && raw[i].nd != wordType::word) RETURN("Expected value of the attribute");
                    t.args[argName] = raw[i].st;
                    i++;
                    skipWhite();
                }


                    //End of tag
                if(raw[i].nd!=wordType::rPar && raw[i].nd!=wordType::rParS) RETURN("Expected ending of tag");
                t.rSlash = raw[i].nd==wordType::rParS;
                i++;
                t.plainText=0;
                res.pb(t);
                continue;
            }

            tag t;
            while(i<raw.size()  && raw[i].nd!=wordType::lPar && raw[i].nd!=wordType::lParS){
                if(raw[i].nd == wordType::space)
                    t.text << token("<SPACE>",true);
                else if(raw[i].nd == wordType::enter)
                    t.text << token("<ENTER>",true);
                else t.text << raw[i].st;
                i++;
            }
            t.plainText=1;
            ampResolution(t.text);
            res.pb(t);



        }
      
        return 1;

    }



        /// @brief Builds a tree
    astNode* treeMake(vector<tag> raw){
        astNode* root = new astNode();
        stack<astNode*> S;
        S.push(root);
        bool pre=0;
        for(auto x:raw){
            // cout<<x.name<<" "<<x.plainText<<" "<<x.text<<endl;

                //Plain Text
            if(x.plainText){
                
                S.top()->addTerminal(x.text,pre);
                continue;
            }
                //Ending tag
            if(x.lSlash){
                if(x.name=="pre") pre=0;
                while(S.size() && !(S.top()->name == x.name || S.top()->name=="h3" && x.name=="h4")){
                    if(S.top()->p == 0){
                        cout<<"NAME MISSMATCH of tag " << x.name << "\n";
                        cout<<"TRace:"<<endl;
                        root->debugPrint();
                        cout<<"end trace"<<endl;
                        return 0;
                    }
                    for(auto x: S.top()->ch){
                        S.top()->p->ch.push_back(x);
                    }
                    S.top()->ch.clear();
                    S.pop();
                }
                if(S.size() == 0){
                    cout<<"NAME MISSMATCH:\n";
                    cout<<"close: "<<x.name<<endl;
                    return 0;
                }
                S.pop();
                continue;
            }
                //Lone tag
            if(x.rSlash){
                
                S.top()->addChild(x.name, x.args);
                continue;
            }

                //Opening tag
            {
                
                if(x.name=="pre") pre=1;
                astNode* n = S.top()->addChild(x.name, x.args);
                n = S.top()->ch.back();
                S.push(n);
                continue;
            }


        }


        return root;

    }






    astNode* all(string link){

        cout<<"LOADING..."<<endl;
        string o1;
        load(link, o1);
        
        cout<<"LEXING..."<<endl;
        vector<pair<string,wordType>> o2;
        lexer(o1, o2);
        for(auto x: o2){
            if(x.st == " ") LOG("lexemes", "(<space>)");
            else if(x.st == "\n") LOG("lexemes", "(<enter>)");
            else LOG("lexemes", "("+x.st+")");
        }
        
        cout<<"PARSING..."<<endl;
        vector<tag> o3;
        parser(o2, o3);
        for(auto t: o3){
            string s = "plain:" + t.text.toStringRaw()+";";
            if(!t.plainText) s = "<"+t.name+">";
            LOG("tags", s);
        }

        cout<<"TREE GROWING..."<<endl;
        astNode* n = treeMake(o3);

        n->debugPrint();
        cout<<"DONE"<<endl;

        return n;
    }

}



