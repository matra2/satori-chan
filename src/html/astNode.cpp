#pragma once
#include "../defines/defines.hpp"
#include "../lib/codefrag.cpp"
#include "astNode.hpp"


namespace htmlAst{

    astNode::astNode(){
        in={};
        name="";
        ch=vector<astNode*>(0);
        terminal=0;
        args={};
        p=0;
    }



    void astNode::debugPrint(int d, int k){
        string line;
        if(terminal==0){
            line += string(d*2,' ') + "<" + name;
            for(auto x:args) line += " " + x.st + "=\"" + x.nd + "\""; 
            line += ">";
            LOG("HtmlTree", line);
            for(int j=0;j<ch.size();j++) ch[j]->debugPrint(d+1,j);
            LOG("HtmlTree", string(d*2,' ')+ "</"+name+">");
        }
        else{
            string s;
            for(auto x:in) s+=x.val;
            s = in.toStringRaw();
            LOG("HtmlTree", string(d*2,' ') + "<["+  s + "]> <!--TERMINAL-->");
        }
    }


    astNode* astNode::addTerminal(codefrag cf, bool isInPre){
        bool isWhite = 1, hasEnter = 0;
        for(auto x:cf){
            if(x == ENT) hasEnter = 1;
            if(!x.isSpec()) for(auto y:x.val) 
                isWhite &= (y == ' ' || y == '\t' || y == '\n'), hasEnter |= y=='\n';
        }
        if(isWhite && !isInPre && hasEnter) return nullptr;

        astNode*n = new astNode();
        n->terminal = 1;
        n->in = cf;
        n->p = this;
        this->ch.push_back(n);

        return n;
    }

    astNode* astNode::addChild(string _name, map<string,string> _args){

        astNode*n = new astNode();
        n->terminal = 0;
        n->p=this;
        n->name = _name;
        n->args = _args;
        this->ch.push_back(n);

        return n;
    }


    void astNode::addOption(string key, int val, bool rec){

        optiones[key] = val;
        if(!rec) return;
        for(auto x: ch) x->addOption(key, val, 1);



    }
        
}

