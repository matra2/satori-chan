#pragma once

#include <map>
#include "../lib/codefrag.hpp"

namespace htmlAst{


    /**
     * @brief a node of AST of html page
     * 
     */
    struct astNode{

    public:

            /// @brief if Node is a tag (otherwise it is plain text)
        bool terminal;

            /// @brief plain text stored in node (is empty if node is a tag)
        codefrag in;

            /// @brief parent
        astNode *p;
        
            /// @brief list of children
        vector<astNode*> ch;

            /// @brief name of the tag (is empty if node is not a tag)
        string name;

            /// @brief list of attributes of the tag (is empty if node is not a tag)
        map<string,string> args;

            /// @brief TODFO
        map<string, int> optiones;
        


    public:

            /// @brief default constructor
        astNode();  

            /// @brief adds new node as child that represents the given codefrag
        astNode* addTerminal(codefrag s, bool isInPre=0);  

            /// @brief adds new node as child
        astNode* addChild(string _name, map<string,string> _args);


        void debugPrint(int d=0, int k=0);

        void addOption(string key, int val, bool rec = 0);


    };

}
