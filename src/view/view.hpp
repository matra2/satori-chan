#pragma once
#include <string>
#include <vector>
#include <map>
#include "../html/astNode.hpp"
#include "../lib/codefrag.hpp"
#include "../defines/defines.hpp"

using namespace std;
using namespace htmlAst;




class pageState{

    public: 

    vector<pair<string,astNode*>> links;

    codefrag content;

    map<string,string> formValues;

    map<string,int> data;

    bool satoriDomain;

    string url;

    bool cached;



};



class view{

    public:

        void openPage(string _url, bool force=0);

        void openPageLocal(string _url);

        void PrintPage();

        bool handleInput();

        void satoriActivities(pageState s, astNode*node);

        view();

        void fetchFromWeb(string name);
        //private:

        pageState state;

        htmlAst::astNode* root;


        vector<string> history;

};



namespace pageBuilder {


    namespace utils {

        static codefrag generate(pageState* state, astNode*node, int width);

        static vector<codefrag> printPage(pageState* state, astNode*node, int width=-1);

            /// @brief returns token of i spaces
        static token space(int i);

            // @brief divides given codefrag where NL is
        static vector<codefrag> partition(codefrag cf);

            /// @brief given codefrag in RAW formation, convetrs it to BLOCK formation
        static codefrag blockify(codefrag cf, int width);

            /// @brief given codefrag in RAW formation that is inside <pre> tag, convetrs it to BLOCK formation
        static codefrag blockifyPre(codefrag cf, int width);

            /// @brief @brief glues results of all children to one line. Returns in RAW formation
        static codefrag accRaw(pageState* state, astNode*node, int width);

            /// @brief @brief glues results of all children to one line. Returns in BLOCK formation
        static codefrag accBlock(pageState* state, astNode*node, int width);

        static codefrag enbox(vector<codefrag> data, int width, vector<vector<string>> style, material color);


        static astNode* findTag(astNode* n, string name, vector<pair<string,string>> args);
        static vector<astNode*> findAllTags(astNode* n, string name, vector<pair<string,string>> args);
        // astNode* findTag(astNode* n, string name, pair<string,string> args);

        // static void addOptionToChildren(astNode* n, string key, int val);
        //
        static int tryProblemStatus(pageState*, astNode*, int, codefrag*);

    };
        #define itag static codefrag generate(pageState* state, astNode*node, int width);
    // struct Itag {
    //     static codefrag generate(pageState* state, astNode*node, int width);
    // };

    namespace main {

        struct head { itag };
        struct body { itag };
        struct div  { itag };

    };

    namespace listing {

       struct ol { itag };
       struct ul { itag };
       struct blockquote { itag };
       struct li { itag };
       struct dd { itag };
       struct dt { itag };
       struct dl { itag };

    };


    namespace tables {

       struct table { itag };
       struct td { itag };
       struct th { itag };
    //    struct tr { itag };
    //    struct thead { itag };
       

            /// @brief returns vector of all <tr> inside this node
        static vector<astNode*> collectTr(astNode*node);

            /// @brief returns table of <tr> inside this node
        static vector<vector<astNode*>> collectTable(astNode*node);

                /// @brief given table of <tr>'s calculates widths if each column
        static vector<int> calcWidths(pageState*state, vector<vector<astNode*>> v, int width, vector<vector<string>> tableStle);


        static codefrag generateTable(pageState*state, vector<vector<astNode*>> content, vector<int> widths, int width, vector<vector<string>> tableStle, material mat);


    };

    namespace text {

        struct h { itag };
        struct h1 { itag };
        struct h2 { itag };
        struct h3 { itag };
        struct h4 { itag };
        struct p { itag };
        struct cite { itag };
        struct em { itag };
        struct code { itag };
        struct strong { itag };
        struct a { itag };
 
    };

    namespace pres {

        struct pre { itag };
        struct span { itag };
        struct img { itag };

    };


    namespace forms {

        struct form { itag };
        struct input { itag };
        struct label { itag };
        struct select { itag };
        struct option { itag };

    };


    namespace domains{

        struct satori {
            vector<astNode*> splitScreen(astNode* n);
        };

    };






};







    const material _col_page = material(termColor::dyellow, 1);
    const material _col_enumerate = material(termColor::yellow, 8);
    const material _col_image = material(termColor::dpurple, 0);

    const material _col_tabOuter = material (termColor::yellow, 9);
    const material _col_preBox = material(termColor::green, 1);

    const material _col_header = material(termColor::blue, 1);
    const material _col_cite = material(termColor::yellow, 2);
    const material _col_strong = material(termColor::white, 1);
    const material _col_math = material(termColor::green, 1);

    const material _col_link = material(termColor::red, 2);
    const material _col_form = material(termColor::red, 3);
    const material _col_form_field = material(termColor::dpurple, 3);

    const material _col_url = material(termColor::red, 1);

    const material _col_OK = material(termColor::green, 1);
    const material _col_ANS = material(termColor::red, 1);
    const material _col_QUE = material(termColor::yellow, 1);
    const material _col_RTE = material(termColor::red, 1);
    const material _col_MEM = material(termColor::blue, 1);
    const material _col_TLE = material(termColor::red, 1);
    const material _col_INT = material(termColor::white, 1);
    const material _col_EXT = material(termColor::blue, 1);
    const material _col_CME = material(termColor::blue, 1);




    const material __s1 = material(termColor::white, 1);
    const material __s2 = material(termColor::red, 1);

    const codefrag LOGO = codefrag(vector<token>{
        (string)"  ┌──────────────────────┐" + __s1, NL,
        (string)"  │                      │" + __s1, NL,
        (string)"  │       L,  L, "+__s1,(string)",J"+__s2,(string)"      │"+__s1, NL,
        (string)"  │    ,i__Z___Z_"+__s1,(string)"S"+__s2,(string)"___i   │"+__s1, NL, 
        (string)"  │   .J  ,______,  J'   │" + __s1,  NL,
        (string)"  │       I______I       │" + __s1, NL,
        (string)"  │       I______I       │" + __s1, NL,
        (string)"  │       I______I       │" + __s1,  NL,
        (string)"  │       \"/   L,\"  i    │"+__s1,  NL,
        (string)"  │    __-J\"   \"L___I    │"+__s1,  NL,
        (string)"  │                      │" + __s1,  NL,
        (string)"  └──────────────────────┘" + __s1
    });
    // const codefrag LOGO = codefrag(vector<token>{
    //     (string)"  +------------------------+" + __s1, NL,
    //     (string)"  |                        |" + __s1, NL,
    //     (string)"  |        L,  L, "+__s1,(string)",J"+__s2,(string)"       |"+__s1, NL,
    //     (string)"  |     ,i__Z___Z_"+__s1,(string)"S"+__s2,(string)"___i    |"+__s1, NL, 
    //     (string)"  |    .J  ,______,  J'    |" + __s1,  NL,
    //     (string)"  |        I______I        |" + __s1, NL,
    //     (string)"  |        I______I        |" + __s1, NL,
    //     (string)"  |        I______I        |" + __s1,  NL,
    //     (string)"  |        \"/   L,\"  i     |"+__s1,  NL,
    //     (string)"  |     __-J\"   \"L___I     |"+__s1,  NL,
    //     (string)"  |                        |" + __s1,  NL,
    //     (string)"  +------------------------+" + __s1
    // });
