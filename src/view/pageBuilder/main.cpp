#pragma once
#include <string>
#include <vector>
#include <map>
#include "../../html/astNode.cpp"
#include "../../lib/codefrag.cpp"
#include "../view.hpp"
#include "../../defines/defines.hpp"

#define DE {\
    string l; \
    for(auto x:res) l+=x.val+",";\
    LOG("building", node->name +":  "+l);\
}

using namespace pageBuilder::utils;

codefrag
pageBuilder::main::div::generate
(pageState *state, astNode *node, int width)
{
    codefrag res;
    if(tryProblemStatus(state, node, width, &res))
        return res << RAW;
    
    
    res = accBlock(state, node, width);
    DE
    return res << BLOCK;

}

codefrag 
pageBuilder::main::body::generate
(pageState* state, astNode*node, int width)
{



    codefrag res;
    material M = _col_page;
    vector<vector<string>> style = pageBorder;


    if(state->satoriDomain){
        int lwidth = 29;
        int pb00 = token(pageBorder[0][0]).displaySize();
        int pb02 = token(pageBorder[0][2]).displaySize();
        int rwidth = width-lwidth-pb00-pb02-2;

        astNode* linkNode = new astNode();
        string urltext = state->url;
        urltext += state->cached ? " (cached)" : " (to date)";
        linkNode->addTerminal(urltext);
        
        astNode* menuNode = new astNode();
        menuNode->ch.pb(pageBuilder::utils::findTag(node, "div", {{"id", "header"}}));
        menuNode->ch.pb(pageBuilder::utils::findTag(node, "div", {{"class", "col2 rDots"}}));
                      

        vector<vector<astNode*>> content = {
            {linkNode}, 
            {menuNode, pageBuilder::utils::findTag(node, "div", {{"class", "col1"}})}
        };
        vector<int> widths = {lwidth, rwidth};
        //  res << "#" + repeat(width-2, '=') + "#" << M << 2 NL;
        // res << "[ " << M << state->url << repeat(width-4 - state->url.size(), ' ') << " ]" << M ;
        res << pageBuilder::tables::generateTable(state, content, widths, width, pageBorder, _col_page);
        DE
        return res;
    }
    vector<codefrag> v;
    v = pageBuilder::utils::partition(pageBuilder::utils::accBlock(state, node, width-4));
    res << "#" + repeat(width-2, '=') + "#" << M <<  NL;
    res << "[ " << M << state->url << repeat(width-4 - state->url.size(), ' ') << " ]" << M ;
    res << "#" + repeat(width-2, '=') + "#" << M <<  NL;
    for(auto x:v)
        res << "[ " << M << x << " ]" << M << NL;
    res << "#" + repeat(width-2, '=') + "#" << M << NL;

    DE


    return res << BLOCK;


}

