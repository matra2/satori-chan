#pragma once
#include <string>
#include <vector>
#include <map>
#include "../../html/astNode.cpp"
#include "../../lib/codefrag.cpp"
#include "../view.hpp"
#include "../../defines/defines.hpp"




codefrag 
pageBuilder::tables::th::generate
(pageState* state, astNode*node, int width)
{
    return td::generate(state, node, width);
}


codefrag 
pageBuilder::tables::td::generate
(pageState* state, astNode*node, int width)
{
    using namespace pageBuilder::utils;
    codefrag res;
    
    if(tryProblemStatus(state, node, width, &res))
        return res << RAW;

    res = pageBuilder::utils::accBlock(state, node, width);
    string s;
    for(auto x:res) s+= x.val;
    LOG("debug", "TD: " + s);
    return res << BLOCK;
}


codefrag 
pageBuilder::tables::table::generate
(pageState* state, astNode*node, int width)
{
    auto tableStle = tableBorder;
    vector<vector<astNode*>> v = collectTable(node);
    vector<int> widths = calcWidths(state, v, width, tableStle);

    return generateTable(state, v, widths, width, tableStle, _col_tabOuter);

}


vector<astNode*>
pageBuilder::tables::collectTr
(astNode*node)
{
    vector<astNode*> res;
    for(auto x:node->ch){
        if(x->name == "td" || x->name == "th") res.pb(x);
    }
    return res;

}

vector<vector<astNode*>> 
pageBuilder::tables::collectTable
(astNode*node)
{
    vector<vector<astNode*>> res;
    for(auto x:node->ch){
        if(x->name == "tbody"){
            auto res2 = collectTable(x);
            for(auto&x:res2)res.pb(x);
        }
        if(x->name == "thead"){
            auto res2 = collectTable(x);
            for(auto&x:res2)res.pb(x);
        }
        if(x->name == "tr") res.pb(collectTr(x));
    }
    int w = 0;
    for(auto x: res) w = max(w, (int)x.size());
    // for(auto&x: res) while(x.size()<w) x.pb(nullptr);
    return res;
}


vector<int> 
pageBuilder::tables::calcWidths
(pageState*state, vector<vector<astNode*>> v, int width, vector<vector<string>> tableStle) 
{
    using namespace pageBuilder::utils;

    
    if(v.size()==0) return {};

    vector<int> borderSizes(4);
    for(int i=0;i<4;i++)
        borderSizes[i] = token(tableStle[0][i]).displaySize();

    int colNum = 0;
    for(auto&x:v) colNum = max(colNum, (int)x.size());

    vector<int> res(colNum, 0);
    vector<int> widths(colNum, 0);

    int linknum = state->links.size();
    for(int j=0;j<v.size();j++){ for(int i=0;i<v[j].size();i++){
            auto w = partition(accBlock(state, v[j][i], -1));
            for(auto ww:w) widths[i] = max(widths[i], ww.displaySize());
        }
    }
    while(state->links.size()>linknum) state->links.pop_back();

    vector<bool> limited(colNum, 0);
    int limitedNum = 0;
    for(bool change=1; change && colNum>limitedNum;){
        change = 0;
        int target = width - borderSizes[0] - borderSizes[3] - borderSizes[2] * (colNum-1);
        for(int i=0;i<colNum;i++) if(limited[i]) target -= widths[i];
        int q = target/(colNum-limitedNum);
        int r = target - q*(colNum-limitedNum);

        for(int i=0;i<colNum;i++) if(!limited[i]){
            res[i] = q + (i<r);
            if(res[i] >= widths[i]) 
                res[i]=widths[i], limited[i]=1, limitedNum++, change=1;
        }

    }



    return res;

}



// string repeat(int i, string ss){
//     token s = ss;
//     if(i<=0) return "";
//     string res;
//     while(i>= s.size()) 
//         res += ss, i-=s.size();
//     res += ss.substr(0, i);
//     return res;
// }


codefrag 
pageBuilder::tables::generateTable
(pageState*state, vector<vector<astNode*>> content, vector<int> widths, int width, vector<vector<string>> style, material color)
{

    codefrag res;

    int ts00 = token(style[0][0]).displaySize();
    int ts02 = token(style[0][2]).displaySize();
    int ts03 = token(style[0][3]).displaySize();

    int styleWidth = ts00 + ts03 + ts02*(widths.size()-1);
    // int styleWidth = style[0][0].size() +  style[0][3].size() +  style[0][2].size()*(widths.size()-1);
    for(auto x:widths) styleWidth += x;

    material FB = color;


    auto genLine = [&](int i){

        material FN(FB.color, 8+(i==0 || i==3));
        codefrag res;

        res << style[i][0] + FB; 
        for(int j=0;j<widths.size();j++) {
            res << repeat(widths[j], style[i][1]) + FN;
            if(j+1<widths.size()) res << style[i][2] + FN;
            else res << style[i][3] + FB;
        }
        
        res << pageBuilder::utils::space(width-styleWidth);
        return res;
    };


    

    for(int i=0;i<content.size();i++){
        auto& row = content[i];

        if(i==0) res << genLine(0) << NL;
        else  res << genLine(2)  << NL;

        vector<vector<codefrag>> cells;

        int oldWidth = widths[row.size()-1];
        if(row.size() < widths.size()){
            for(int i=row.size(); i<widths.size();i++)
                widths[row.size()-1] += widths[i] + 3;
        }

        for(int i=0;i<row.size();i++)
            cells.pb(pageBuilder::utils::partition(pageBuilder::utils::generate(state, row[i], widths[i])));
        
        int h=0;
        for(auto y:cells) h = max(h, (int)y.size());

        for(int i=0;i<h;i++){

            res << style[1][0] << FB;

            for(int cell=0;cell<cells.size();cell++){

                if(cells[cell].size()>i) res << cells[cell][i];
                else res << pageBuilder::utils::space(widths[cell]);

                if(cell+1==cells.size()) res << style[1][3] + FB;
                else res << style[1][2] + material(FB.color, 8);
            }
            res << pageBuilder::utils::space(width-styleWidth);
            res << NL;
        }

        widths[row.size()-1] = oldWidth;
    }
    // cout<<"ASAAAAAAAAAAAAAAAAAAAAa"<<endl;
    // res <<NON;
    res << genLine(3) << NL;
    //   res << "A"; 
    //   res <<NON<< "┗━"<<NL; 
    //   res << "B"<<NL; 

    return res << BLOCK;


}
