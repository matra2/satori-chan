#pragma once
#include <string>
#include <vector>
#include <map>
#include "../../html/astNode.cpp"
#include "../../lib/codefrag.cpp"
#include "../view.hpp"
#include "../../defines/defines.hpp"




codefrag 
pageBuilder::listing::ol::generate
(pageState* state, astNode*node, int width){

    int num = 1;
    codefrag res;

    if(node->args["start"] != "") num= stoi(node->args["start"]);
    for(auto x:node->ch) if(x->name == "li")
        x->args["__num"] = ((num<10) ? " " : "") + to_string(num) + " ", num++;

    

    auto v = pageBuilder::utils::partition(pageBuilder::utils::accBlock(state, node, width-4));

    for(auto x:v)
        res << TAB << x << NL;
    res << pageBuilder::utils::space(width) << NL;

    // DE
    return res << BLOCK;


}




codefrag 
pageBuilder::listing::ul::generate
(pageState* state, astNode*node, int width){

    using namespace pageBuilder::utils;

    codefrag res;
    for(auto x:node->ch) if(x->name == "li")
        x->args["__num"] = " > ";


    auto v = partition(accBlock(state, node, width-4));


    if(node->args["satoriOptiones"] == "contestList"){
        cout<<"SSSSSSSSSSSSSSSSSSSSSSSS"<<endl;

        astNode*node2 = htmlAst::all(RUNTIME_DIR + "/contestList.html");
        for(auto x:node2->ch) if(x->name == "li")
            x->args["__num"] = " > ";
        v = partition(accBlock(state, node2, width-4));

    }






    for(auto x:v)
        res << TAB << x << NL;
    res <<  space(width) << NL;

    // DE
    return res << BLOCK;



}




codefrag 
pageBuilder::listing::li::generate
(pageState* state, astNode*node, int width){

    codefrag res;
    auto v =  pageBuilder::utils::partition( pageBuilder::utils::accBlock(state,node,width-3));
    material M = _col_enumerate;

    for(int i=0;i<v.size();i++){
        if(i==0){
            res << node->args["__num"] + _col_enumerate;
        }
        else res << "   ";
        res << v[i] << NL;
    }
    if(node->args["__num"] != " > ") res <<  pageBuilder::utils::space(width) << NL;

    // DE
    return res << BLOCK;

}




codefrag 
pageBuilder::listing::blockquote::generate
(pageState* state, astNode*node, int width){

    codefrag res;
    auto v = pageBuilder::utils::partition(pageBuilder::utils::accBlock(state, node, width-4));

    for(auto x:v)
        res << TAB << x << NL;
    res << pageBuilder::utils::space(width) << NL;

    return res << BLOCK;

}


codefrag 
pageBuilder::listing::dl::generate
(pageState* state, astNode*node, int width){

    codefrag res;
    res = pageBuilder::utils::accBlock(state, node, width);
    return res << BLOCK;

}

codefrag 
pageBuilder::listing::dt::generate
(pageState* state, astNode*node, int width){

    codefrag res;
    auto v = pageBuilder::utils::partition(pageBuilder::utils::accBlock(state,node,width-4));

    for(int i=0;i<v.size();i++)
        res << TAB << v[i] << NL;

    return res << BLOCK;

}


codefrag 
pageBuilder::listing::dd::generate
(pageState* state, astNode*node, int width){

    codefrag res;
    auto v = pageBuilder::utils::partition(pageBuilder::utils::accBlock(state,node,width-8));

    for(int i=0;i<v.size();i++)
        res << TAB << TAB << v[i] << NL;

    return res << BLOCK;

}


