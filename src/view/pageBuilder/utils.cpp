#pragma once
#include <string>
#include <vector>
#include <map>
#include "../../html/astNode.cpp"
#include "../../lib/codefrag.cpp"
#include "../view.hpp"

#include <sys/ioctl.h>
#include <unistd.h>

vector<codefrag>
pageBuilder::utils::printPage
(pageState* state, astNode*node, int width)
{

    
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

    if(width<=0) width=w.ws_col;

    cout<<"[pageBuilder::utils::printPage]: generating page..."<<endl;
    vector<codefrag> s = partition(generate(state, node, width));
    cout<<"[pageBuilder::utils::printPage]: generated"<<endl;

    // for(auto x:s) for(auto y:x){
    //     LOG("res", y.val + 
    //         ", \tcol: " + toString(y.mat.color));
    // }
    
    return s;

}


vector<codefrag> 
pageBuilder::utils::partition
(codefrag cf)
{

    if(cf.size()==0) return {};
    vector<codefrag> res;
    res.pb({});

    for(auto x:cf){
        if(x==NL) res.pb({});
        else res.back() << x;
    }
    while(res.size()){
        bool hasAlpha = 0;
        for(auto x:res.back())
            if(!x.isSpec()) for(auto y:x.val) hasAlpha |= !isspace(y);
        if(hasAlpha) break;
        res.pop_back();
    }
    // if(res.back().size()==0) res.pop_back();
    return res;


}

#define DE {\
    string l; \
    for(auto x:res) l+=x.val+",";\
    LOG("building", node->name +":  "+l);\
}



codefrag 
pageBuilder::utils::generate
(pageState* state, astNode*node, int width){

    // cout<<"into: "<<node->name<<endl;


    if(node->name == "body")
        return pageBuilder::main::body::generate(state, node, width);
    if(node->name == "div") 
        return pageBuilder::main::div::generate(state, node, width);
    
    if(node->name == "h")
        return pageBuilder::text::h::generate(state, node, width);
    if(node->name == "h1")
        return pageBuilder::text::h1::generate(state, node, width);
    if(node->name == "h2")
        return pageBuilder::text::h2::generate(state, node, width);
    if(node->name == "h3")
        return pageBuilder::text::h3::generate(state, node, width);
    if(node->name == "h4")
        return pageBuilder::text::h4::generate(state, node, width);
    if(node->name == "p")
        return pageBuilder::text::p::generate(state, node, width);
    if(node->name == "cite")
        return pageBuilder::text::cite::generate(state, node, width);
    if(node->name == "em")
        return pageBuilder::text::em::generate(state, node, width);
    if(node->name == "code")
        return pageBuilder::text::code::generate(state, node, width);
    if(node->name == "strong")
        return pageBuilder::text::strong::generate(state, node, width);
    if(node->name == "a")
        return pageBuilder::text::a::generate(state, node, width);


    if(node->name == "form")
        return pageBuilder::forms::form::generate(state, node, width);
    if(node->name == "label")
        return pageBuilder::forms::label::generate(state, node, width);
    if(node->name == "input")
        return pageBuilder::forms::input::generate(state, node, width);
    if(node->name == "select")
        return pageBuilder::forms::select::generate(state, node, width);
    if(node->name == "option")
        return pageBuilder::forms::option::generate(state, node, width);


    if(node->name == "span")
        return pageBuilder::pres::span::generate(state, node, width);
    if(node->name == "pre")
        return pageBuilder::pres::pre::generate(state, node, width);
    if(node->name == "img")
        return pageBuilder::pres::img::generate(state, node, width);


    if(node->name == "ol")
        return pageBuilder::listing::ol::generate(state, node, width);
    if(node->name == "ul")
        return pageBuilder::listing::ul::generate(state, node, width);
    if(node->name == "li")
        return pageBuilder::listing::li::generate(state, node, width);
    if(node->name == "dl")
        return pageBuilder::listing::dl::generate(state, node, width);
    if(node->name == "dt")
        return pageBuilder::listing::dt::generate(state, node, width);
    if(node->name == "dt")
        return pageBuilder::listing::dt::generate(state, node, width);


    if(node->name == "td")
        return pageBuilder::tables::td::generate(state, node, width);
    if(node->name == "th")
        return pageBuilder::tables::th::generate(state, node, width);
    if(node->name == "table")
        return pageBuilder::tables::table::generate(state, node, width);


    if(node->terminal){
        codefrag res;

        if(node->in.size()==0)
            return res << RAW;
        if(node->optiones["pre"]) {
            for(auto x: node->in){
                if(x==ENT)  res << NL;
                else res << x;
            }
            // DE
            return res << RAW;
        }
        bool wasSpace = 0;
        for(auto x: node->in){
            if(!x.isSpec()){
                res << x;
                wasSpace=0;
            }
            if((x == SPC || x == ENT) && !wasSpace) 
                res << " ", wasSpace=1;
        }
        // DE
        return res << RAW;
    }


    codefrag res = accBlock(state, node, width);
    DE
    return res << BLOCK;

}


int 
pageBuilder::utils::tryProblemStatus
(pageState* state, astNode* node, int width, codefrag* res)
{

    if(!state->satoriDomain)
        return 0;

    pageState stateClone = *state;
    codefrag raw = pageBuilder::utils::accBlock(&stateClone, node, width);

    map<string, pair<bool, material>> statusMap = {
        {"staOK",  {1,_col_OK }},
        {"staANS", {1,_col_ANS}},
        {"staQUE", {1,_col_QUE}},
        {"staRTE", {1,_col_RTE}},
        {"staTLE", {1,_col_TLE}},
        {"staMEM", {1,_col_MEM}},
        {"staINT", {1,_col_INT}},
        {"staEXT", {1,_col_EXT}},
        {"staCME", {1,_col_CME}},
    };

    pair<bool,material> mat = statusMap[node->args["class"]];
    if(!mat.first) return 0;

    *res<< raw + mat.nd << RAW;
    return 1;

    // if( state->satoriDomain && node->args["class"] == "staOK" ){
    //     *res << raw + _col_OK <<RAW;
    //     return 1;
    // }
    // if( state->satoriDomain && node->args["class"] == "staANS" ){
    //     *res << "ANS" << _col_ANS << space(width-3)<< RAW;
    //     return 1;
    // }
    // if( state->satoriDomain && node->args["class"] == "staQUE" ){
    //     *res << "QUEUE" << _col_QUE <<space(width-5)<< RAW;
    //     return 1;
    // }
    // if( state->satoriDomain && node->args["class"] == "staRTE" ){
    //     *res << "RTE" << _col_RTE <<space(width-3)<< RAW;
    //     return 1;
    // }
    // if( state->satoriDomain && node->args["class"] == "staTLE" ){
    //     *res << "TLE" << _col_TLE <<space(width-3)<< RAW;
    //     return 1;
    // }
    // if( state->satoriDomain && node->args["class"] == "staMEM" ){
    //     *res << "MEM" << _col_MEM <<space(width-3)<< RAW;
    //     return 1;
    // }
    // if( state->satoriDomain && node->args["class"] == "staINT" ){
    //     *res << "INT" << _col_INT <<space(width-3)<< RAW;
    //     return 1;
    // }
    // if( state->satoriDomain && node->args["class"] == "staEXT" ){
    //     *res << "EXT" << _col_EXT <<space(width-3)<< RAW;
    //     return 1;
    // }
    // if( state->satoriDomain && node->args["class"] == "staCME" ){
    //     *res << "CME" << _col_CME<<space(width-3)<< RAW;
    //     return 1;
    // }
    
    return 0;
}




codefrag 
pageBuilder::utils::accBlock
(pageState* state, astNode*node, int width)
{
    codefrag res;
    codefrag acc;

    for(auto x: node->ch){
        codefrag temp = generate(state, x, width);
        // if(node->name == "div")LOG("debug", "ACC PART:\t" + temp.toString());
        if(temp.back() == RAW) 
            acc << temp, acc.pop();
        else{
            if(acc.size()>0){
                res << blockify(acc, width) << NL;
                acc = {};
            }
            res << temp, res.pop();
        }
    }
    if(acc.size()>0)
        res << blockify(acc, width) << NL;

    //  if(node->name == "div")LOG("debug", "ACC ALL:\t" + res.toString());
    return res;
}




codefrag
pageBuilder::utils::blockify
(codefrag cf, int width)
{

    if(width<0 || cf.size()==0) return cf;
    if(cf.back()==BLOCK){
        cf.pop();
        return cf;
    }
    
    codefrag res;
    int len=0;
    for(auto x:cf){
        if(x==ENT || x==SPC) continue;
        if(len + x.displaySize() > width)
            res << space(width-len) << NL, len=0;
         if(!(len==0 && x.size()>0 && x.val[0]==' '))                 //why would i do that !?!?! 
            res << x, len+=x.displaySize();                                 // now I know - not to print spaces at the start of a line
    }
    res << space(width-len);
    // LOG("debug", "BLOCKIFY IN :\t" + cf.toString());
    // LOG("debug", "BLOCKIFY OUT :\t" + res.toString());
    return res;


}



token 
pageBuilder::utils::space
(int i)
{
    if(i<=0) return NON;
    return {string(i, ' '), {termColor::white,8}};
}



codefrag 
pageBuilder::utils::accRaw
(pageState* state, astNode*node, int width)
{

    codefrag res;

    for(auto x:node->ch){
        codefrag temp = generate(state, x, width);
        if(temp.back()==BLOCK)
            LOG("error", "shaper:accRaw: BLOCK where not; " + x->name +  " in " + node->name), LOG("error", temp.toString());
        temp.pop();
        res << temp;
    }

    return res;

}


codefrag 
pageBuilder::utils::blockifyPre
(codefrag cf, int width)
{
    if(width<0) return cf;
    codefrag res;
    int len=0;
    for(auto x:cf){
        if(x==SPC) x = token((string)" ");
        if(len + x.displaySize() > width || x==ENT || x==NL)
            res << NL, len=0;
        if(x==ENT || x==NL) continue;
        res << x, len+=x.displaySize();
    }
    if(res.back()==NL) res.pop();

    return res;
}


astNode* 
pageBuilder::utils::findTag
(astNode* n, string name, vector<pair<string,string>> args)
{

    bool good = n->name == name;
    for(auto x: args) good &= (n->args[x.st]==x.nd);
    if(good) return n;

    for(auto x:n->ch){
        astNode* a = findTag(x, name, args);
        if(a) return a;
    }
    
    return nullptr;


}



vector<astNode*> 
pageBuilder::utils::findAllTags
(astNode* n, string name, vector<pair<string,string>> args)
{
    vector<astNode*> res;

    bool good = n->name == name;
    for(auto x: args) good &= (n->args[x.st]==x.nd);
    if(good) res.pb(n);

    for(auto x:n->ch){
        vector<astNode*> a = findAllTags(x, name, args);
        for(auto x: a) res.push_back(x);
    }
    
    return res;


}



codefrag 
pageBuilder::utils::enbox
(vector<codefrag> data, int width, vector<vector<string>> style, material color)
{
    codefrag res;

    res << style[0][0] + color;
    res << repeat(width, style[0][1]) + color;
    res << style[0][3] + color;
    res << NL;

    for(auto x: data){
        res << style[1][0] + color;
        res << x;
        res << space(width-x.displaySize());
        res << style[1][3] + color;
        res << NL;
    }

    res << style[3][0] + color;
    res << repeat(width, style[3][1]) + color;
    res << style[3][3] + color;
    res << NL;

    return res;

}
