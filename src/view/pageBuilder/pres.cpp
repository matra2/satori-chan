#pragma once
#include <string>
#include <vector>
#include <map>
#include "../../html/astNode.cpp"
#include "../../lib/codefrag.cpp"
#include "../view.hpp"
#include "../../defines/defines.hpp"
#include "../bigLetter.hpp"



codefrag 
pageBuilder::pres::pre::generate
(pageState* state, astNode*node, int width)
{   
    using namespace pageBuilder::utils;
    node->addOption("pre", 1, 1);
    
    codefrag res;
    codefrag processed, raw = accRaw(state, node, width);
    for(auto x:raw){
        if(x==SPC) processed << space(x.rep);
        // if(x==SPC) processed << space(x.rep);
        //else if(x==" ") continue;
        else processed << x;
    } 
    
    int pb0 = token(preBorder[0][0]).displaySize() + token(preBorder[0][3]).displaySize();
    codefrag bl = blockifyPre(processed, width-pb0);
    auto v = partition(bl);
    int maxWidth = 4;
    for(auto x:v) maxWidth=max(maxWidth, x.displaySize());
    material M = _col_preBox;

    string index = "[" + to_string(state->links.size()) + "]";
    state->links.pb({bl.toString(), node});
    // res << "/" + repeat(maxWidth+2-index.size(), '-') + index + "\\" << M << space(width-4-maxWidth)<< NL;
    res <<  preBorder[0][0]  + repeat(maxWidth-index.size()-pb0+4, preBorder[0][1]) + index + preBorder[0][3] << M << space(width-maxWidth-4)<< NL;
    for(auto x:v) res << preBorder[1][0] + M << x << space(maxWidth-x.displaySize()) << preBorder[1][3] + M << space(width-maxWidth-4) << NL;
    res << preBorder[3][0] + repeat(maxWidth-pb0+4, preBorder[3][1]) + preBorder[3][3] << M << space(width-4-maxWidth)<< NL;
    res <<  space(width) << NL;

    // res << enbox(v, maxWidth, preBorder, _col_preBox);

    DE
    return res << BLOCK;

}



codefrag 
pageBuilder::pres::img::generate
(pageState* state, astNode*node, int width)
{   
    using namespace pageBuilder::utils;
    codefrag res;
    if(state->satoriDomain && node->args["src"] == "/files/satori_banner.png"){
        // for(auto x:LOGO) res << x << space(width -28) << NL;
        auto c = partition(LOGO);
        for(auto x:c) res << x << space(width - x.displaySize()) << NL; 
        res << space(width) << NL; 
        return res << BLOCK;
    }
    
    res << "[" + to_string(state->links.size()) + " image]" + _col_image;
    state->links.pb({node->args["src"], node});
    res << space(width) << NL; 
    return res << RAW;

}



codefrag 
pageBuilder::pres::span::generate
(pageState* state, astNode*node, int width)
{   

    codefrag res;

    if(node->args["class"]=="math"){

        if(node->ch.size()==0) return {};
        codefrag rawCode = pageBuilder::utils::accRaw(state, node, width);
        string rawText;
        for(auto x:rawCode) rawText += x.val;

        // res << rawText + material(DPURPLE, 6);
        // return res << RAW;
        
        codefrag tokenized;
        string s;
        for(int i=0;i<rawText.size();i++){
            s="";
            if(isspace(rawText[i])) continue;
            if(rawText[i]=='\\'){
                i++, s+=rawText[i];
                if(!isalpha(rawText[i])){
                    tokenized << latexCommand[s];
                    continue;
                }
                while(i+1<rawText.size() && isalpha(rawText[i+1])) i++, s+=rawText[i];
                tokenized << latexCommand[s];
                continue;
            }
            if(isalpha(rawText[i])){
                s+=rawText[i];
                while(i+1<rawText.size() && isalpha(rawText[i+1])) i++, s+=rawText[i];
                tokenized << s;
                continue;
            }
            if(isdigit(rawText[i]) || rawText[i]=='-'&&i+1<rawText.size()&&isdigit(rawText[i+1])){
                s+=rawText[i];
                while(i+1<rawText.size() && isdigit(rawText[i+1])) i++, s+=rawText[i];
                tokenized << s;
                continue;
            }
            s+=rawText[i];
            tokenized << s;
        }

        codefrag temp;
        for(int i=0;i<tokenized.size();i++){
            if(tokenized[i]=="{" && i+2<tokenized.size() && tokenized[i+2]=="}")
                temp << tokenized[i+1], i=i+2;
            else    
                temp << tokenized[i];
        }
        tokenized = temp, temp.clear();

        // return tokenized << RAW;
        temp << token();
        for(int i=0;i<tokenized.size();i++){
            if(tokenized[i]==",") temp.back().val+=tokenized[i].val+" ", temp<<token();
            else if(tokenized[i]=="+" || tokenized[i]=="-" || tokenized[i]=="<" || tokenized[i]=="<=" || tokenized[i]=="=") 
                temp << tokenized[i] << token();
            else temp.back().val += tokenized[i].val;
        }

        temp += _col_math;
        return temp << RAW;
        //return temp << " " << RAW;
    }
    else{
        string c = node->args["class"];
        termColor color = termColor::normal;

        if(c=="p") color = termColor::yellow;       //nawiasy ,;
        if(c=="n") color = termColor::normal;       //domyslne
        if(c=="na") color = termColor::dgreen;     //funkcja
        if(c=="k") color = termColor::dred;        //new
        if(c=="o") color = termColor::dyellow;        //operator
        if(c=="mi") color = termColor::dyellow;        //stała
        if(c=="s") color = termColor::dwihte;        //string
        if(c=="kn") color = termColor::dred;        //import
        if(c=="nn") color = termColor::dwihte;        //pacage
        if(c=="kd") color = termColor::dred;        //public, class
        if(c=="nc") color = termColor::dgreen;        //nazwa  Ancient κ-solutionsklasy
        if(c=="nd") color = termColor::white;        //@
        if(c=="kt") color = termColor::dyellow;        //typ
        if(c=="nf") color = termColor::dgreen;        //definicja funkcji
        res = pageBuilder::utils::accRaw(state, node, width);

        /*
                c++

            cp      (#include)
            scp     (<ios...>,  "dddd")
            k       keywodr (auto, cost, for)
            kt      keyword type (int, char)
            mi      literal int (0, 2137)
            n       name
            nb      name bool (true, false)
            nf      name function (main)
            nl      name ?
            o       operator (, --, ., ::)
            p       punctuation ( (, ), ;, ,, :, {, [, )
            pre     ? types, idents, names
            s       string ("aaa")
            sc      char ? ('s')
            se      string escape (\n)
            
                rust
            
            bp      (self)
            c1      comment (//cc)
            cp      (#[derive(...)], #include)
            nb      (None, Option, Result)
            nc      name class (SBuilder)
            nf      name function 
            signature (2023-11-27) ???
        */




        LOG("span", "class:\t" + c + "\tname:\t" + res.toString()); 
        res += material(color);//TODO


        return res << RAW;
    }
    

}
