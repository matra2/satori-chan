#pragma once
#include <string>
#include <vector>
#include <map>
#include "../../html/astNode.cpp"
#include "../../lib/codefrag.cpp"
#include "../view.hpp"
#include "../../defines/defines.hpp"
#include "../bigLetter.hpp"

using namespace pageBuilder::forms;

codefrag 
form::generate
(pageState* state, astNode*node, int width)
{
    codefrag res = pageBuilder::utils::accBlock(state, node, width);
    return res << BLOCK;
}



codefrag 
pageBuilder::forms::input::generate
(pageState* state, astNode*node, int width)
{
    codefrag res;

    if(node->args["type"] == "text"){
        res << "[" + to_string(state->links.size()) + "]";
        res << _col_link;
        state->links.pb({node->args["name"], node});
        res << "[" +state->formValues[node->args["name"]] + "]";
        res << _col_form;
    }
    
    if(node->args["type"] == "password"){
        res << "[" + to_string(state->links.size()) + "]";
        res << _col_link;
        state->links.pb({node->args["name"], node});
        res << "[" +repeat(state->formValues[node->args["name"]].size(), '*') + "]";
        res << _col_form;
    }

    if(node->args["type"] == "file"){
        res << "[" + to_string(state->links.size()) + "]";
        res << _col_link;
        state->links.pb({node->args["name"], node});
        res << "[" +state->formValues[node->args["name"]] + "]";
        res << _col_preBox;
    }

    if(node->args["class"] == "button"){
        res << "[" + to_string(state->links.size()) + "]";
        res += _col_link;
        state->links.pb({node->args["name"], node});
        res << "[" + node->args["value"] + "]";
        res <<_col_form_field;
    }
    
    return res << RAW;
}



codefrag 
pageBuilder::forms::label::generate
(pageState* state, astNode*node, int width)
{
    codefrag res;
    res << pageBuilder::utils::accRaw(state, node, width) << RAW;
    return res;
}





codefrag 
pageBuilder::forms::select::generate
(pageState* state, astNode*node, int width)
{
    using namespace pageBuilder::utils; 

    codefrag res;
    node->addOption("selectParent", state->links.size(), 1);
    state->links.pb({node->args["name"], node});
    if(state->data[node->args["name"]] == 0){
        res << "[" + to_string(state->links.size()-1) + "]";
        res += _col_link;
        res << "[" << _col_preBox << accRaw(state, node, width)  + _col_preBox << "]"<< _col_preBox;
        res <<_col_preBox;
        return res << RAW;
    }
    else{
        auto v = partition(accBlock(state, node, width-8));
        res << enbox(v, width-4, preBorder, _col_preBox);
        // res << pageBuilder::pres::pre::generate(state, node, width);
        return res << BLOCK;
    }
    return {RAW};
}


codefrag 
pageBuilder::forms::option::generate
(pageState* state, astNode*node, int width)
{
    using namespace pageBuilder::utils; 
    
    codefrag res;
    string parentName = state->links[node->optiones["selectParent"]].st;
    if(state->data[parentName] == 0){
        if(node->args["selected"] != "selected")
            return {RAW};
        res << pageBuilder::utils::accRaw(state, node, width);
        return res << RAW;
    }
    else{
        string linktext = "[" + to_string(state->links.size()) + "] ";
        // state->data["parentName" + to_string(state->links.size())] = 
        res << linktext << _col_link;
        state->links.pb({node->args["href"], node});
        auto v = accRaw(state, node, width);
        res << v << space(width - v.displaySize() - linktext.size()) << NL;
        return res << BLOCK;
    }

    return {RAW};
}


