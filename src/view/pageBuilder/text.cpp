#pragma once
#include <string>
#include <vector>
#include <map>
#include "../../html/astNode.cpp"
#include "../../lib/codefrag.cpp"
#include "../view.hpp"
#include "../../defines/defines.hpp"
#include "../bigLetter.hpp"



codefrag 
pageBuilder::text::h::generate
(pageState* state, astNode*node, int width)
{
    material MAT = _col_header;

    codefrag res;
    codefrag raw = pageBuilder::utils::accRaw(state, node, (width-4));
    
    auto proc = bigLetter::generate(raw.toString(), width-4);
    for(auto x:proc) res << TAB << x + MAT << NL;
    return res << BLOCK;
}



codefrag 
pageBuilder::text::h1::generate
(pageState* state, astNode*node, int width)
{
    return pageBuilder::text::h::generate(state, node, width);
}



codefrag 
pageBuilder::text::h2::generate
(pageState* state, astNode*node, int width)
{
    using namespace   pageBuilder::utils;

    codefrag res;
    codefrag acc = accRaw(state, node, width);
    res << TAB << " >>> " << _col_header;
    res << acc + _col_header;
    res << " <<< " << _col_header;
    res << space(width-14-acc.displaySize()) << NL << space(width) << NL;
    return res << BLOCK;
    // return pageBuilder::text::strong::generate(state, node, width);
}



codefrag 
pageBuilder::text::h3::generate
(pageState* state, astNode*node, int width)
{
    return pageBuilder::text::h2::generate(state, node, width);
}



codefrag 
pageBuilder::text::h4::generate
(pageState* state, astNode*node, int width)
{
    return pageBuilder::text::strong::generate(state, node, width);
}



codefrag 
pageBuilder::text::p::generate
(pageState* state, astNode*node, int width)
{   
    codefrag res;
    codefrag c = pageBuilder::utils::accRaw(state, node, width);
    auto v = pageBuilder::utils::partition(pageBuilder::utils::blockify(c, width));
    for(auto x:v) res << x << NL;
    res << pageBuilder::utils::space(width) << NL;
    return res << BLOCK;
}



codefrag 
pageBuilder::text::cite::generate
(pageState* state, astNode*node, int width)
{   
    codefrag res;
    res = pageBuilder::utils::accRaw(state, node, width);
    res = res + _col_cite;
    return res << RAW;
}



codefrag 
pageBuilder::text::em::generate
(pageState* state, astNode*node, int width)
{
    return pageBuilder::text::cite::generate(state, node, width);
}



codefrag 
pageBuilder::text::code::generate
(pageState* state, astNode*node, int width)
{
    return pageBuilder::text::cite::generate(state, node, width);
}



codefrag 
pageBuilder::text::strong::generate
(pageState* state, astNode*node, int width)
{   
    codefrag res;
    res = pageBuilder::utils::accRaw(state, node, width);
    res = res + _col_strong;
    return res << RAW;
}



codefrag 
pageBuilder::text::a::generate
(pageState* state, astNode*node, int width)
{
    if(node->args["class"]=="headerlink") return {RAW};
    if(node->args["satoriOptiones"]=="lastPage"){
        ifstream in;
        in.open(RUNTIME_DIR + "/link.txt");
        string link;
        in >> link;
        in.close();
        node->args["href"] = link;
        cout<<"THELINKL "<<link<<endl;
    }

    material F = _col_link;
    codefrag res;

    res << "[" + to_string(state->links.size()) + "]" << F;
    state->links.pb({node->args["href"], node});
    res << " " << pageBuilder::utils::accRaw(state, node, width);
    // DE
    return res << RAW;
}

