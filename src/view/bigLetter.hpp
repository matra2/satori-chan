#pragma once
#include<iostream>
#include<vector>
#include<string>

using namespace std;

namespace bigLetter{


	vector<string> asciiClassic_4x6 = {

		"    "
		"    "
		"    "
		"    "
		"    "
		"    ",		

		" xx "
		" xx "
		" xx "
		" xx "
		"    "
		" xx ",

		"x x "
		"x x "
		"    "
		"    "
		"    "
		"    ",

		" xx "
		"xxxx"
		" xx "
		" xx"
		"xxxx"
		" xx ",

		"  x  "
		"xxxx"
		" x  "
		"  x "
		"xxxx"
		"  x ",

		"x x "
		"  x "
		" x  "
		" x  "
		"x   "
		"x x ",

		" x  "
		"xxx "
		"xx  "
		" xxx"
		"xx x"
		" xx ",

		" x  "
		" x  "
		"    "
		"    "
		"    "
		"    ",
					////////////////////
		" x  "
		"x   "
		"x   "
		"x   "
		"x   "
		" x  ",

		"  x "
		"   x"
		"   x"
		"   x"
		"   x"
		"  x ",

		"    "
		"x x "
		" x  "
		"x x "
		"    "
		"    ",

		"    "
		"    "
		" x  "
		"xxx "
		" x  "
		"    ",

		"    " 
		"    "
		"    "
		"    "
		" x  "
		" x  ",

		"    "
		"    "
		"    "
		"xxx "
		"    "
		"    ",

		"    "
		"    "
		"    "
		"    "
		"    "
		" x  ",

		"  x "  
		"  x "
		" x  "
		" x  "
		"x   "
		"x   ",
					//////////////////////
		" xx "
		"x xx"
		"x xx"
		"xx x"
		"xx x"
		" xx ",

		" x  " 
		"xx  "
		" x  "
		" x  "
		" x  "
		"xxx ",

		" xx "
		"x  x"
		"   x"
		"  x "
		" x  "
		"xxxx",

		" xx "
		"x  x"
		"  x "
		"   x"
		"x  x"
		" xx ",

		"x  x"
		"x  x"
		"xxxx"
		"   x"
		"   x"
		"   x",

		"xxxx"
		"x   "
		"xxx "
		"   x"
		"   x"
		"xxx ",

		" xxx"
		"x   "
		"xxx "
		"x  x"
		"x  x"
		" xx ",

		"xxxx"
		"   x"
		"   x"
		"   x"
		"   x"
		"   x",
					//////////////////////////////////
		
		" xx "
		"x  x"
		" xx "
		"x  x"
		"x  x"
		" xx ",

		" xx "
		"x  x"
		" xxx"
		"   x"
		"   x"
		"   x",

		"    "
		"    "
		" x  "
		"    "
		" x  "
		"    ",

		"    "
		"    "
		" x  "
		"    "
		" x  "
		" x  ",

		"    "
		"  x "
		" x  "
		"x   "
		" x  "
		"  x ",

		"    "
		"    "
		"xxx "
		"    "
		"xxx "
		"    ",

		"    "
		" x  "
		"  x "
		"   x"
		"  x "
		" x  ",

		"xxx "
		"   x"
		" xx "
		" x  "
		"    "
		" x  ",
				////////////////////////////////

		"    "
		" xx "
		"x  x"
		"x xx"
		"x   "
		" xxx",

		" xx "
		"x  x"
		"x  x"
		"xxxx"
		"x  x"
		"x  x",

		"xxx "
		"x  x"
		"xxx "
		"x  x"
		"x  x"
		"xxx ",

		" xx "
		"x  x"
		"x   "
		"x   "
		"x  x"
		" xx ",

		"xxx "
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		"xxx ",

		"xxxx"
		"x   "
		"xxx "
		"x   "
		"x   "
		"xxxx",

		"xxxx"
		"x   "
		"xxx "
		"x   "
		"x   "
		"x   ",

		" xx "
		"x  x"
		"x   "
		"x xx"
		"x  x"
		" xx ",
					/////////////////////////////////////
		
		"x  x"
		"x  x"
		"xxxx"
		"x  x"
		"x  x"
		"x  x",
		
		"xxx "
		" x  "
		" x  "
		" x  "
		" x  "
		"xxx ",

		"   x"
		"   x"
		"   x"
		"   x"
		"x  x"
		" xx ",

		"x  x"
		"x x "
		"xx  "
		"xx  "
		"x x "
		"x  x",

		"x   "
		"x   "
		"x   "
		"x   "
		"x   "
		"xxxx",

		"x  x"
		"xxxx"
		"xxxx"
		"x  x"
		"x  x"
		"x  x",

		"x  x"
		"xx x"
		"xx x"
		"x xx"
		"x xx"
		"x  x",

		" xx "
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		" xx ",
			/////////////
		"xxx "
		"x  x"
		"x  x"
		"xxx "
		"x   "
		"x   ",

		" xx "
		"x  x"
		"x  x"
		"x  x"
		" xx "
		"   x",

		"xxx "
		"x  x"
		"x  x"
		"xxx "
		"x x "
		"x  x",

		" xx "
		"x  x"
		" x  "
		"  x "
		"x  x"
		" xx ",

		"xxx "
		" x  "
		" x  "
		" x  "
		" x  "
		" x  ",

		"x  x"
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		" xx ",

		"x  x"
		"x  x"
		"x  x"
		"x  x"
		" xx "
		" xx ",

		"x  x"
		"x  x"
		"x  x"
		"xxxx"
		"xxxx"
		"x  x",
			/////////////
		"x  x"
		"x  x"
		" xx "
		" xx "
		"x  x"
		"x  x",

		"x x "
		"x x "
		"x x "
		" x  "
		" x  "
		" x  ",

		"xxx "
		"  x "
		" x  "
		" x  "
		"x   "
		"xxx ",

		"xx  "
		"x   "
		"x   "
		"x   "
		"x   "
		"xx  ",

		"x   "
		"x   "
		" x  "
		" x  "
		"  x "
		"  x ",

		"  xx"
		"   x"
		"   x"
		"   x"
		"   x"
		"  xx",

		" x  "
		"x x "
		"x x "
		"    "
		"    "
		"    ",

		"    "
		"    "
		"    "
		"    "
		"    "
		"xxxx",
			/////////////
		" x  "
		"  x "
		"    "
		"    "
		"    "
		"    ",

		"    "
		"xxx "
		"   x"
		"xxxx"
		"x  x"
		" xxx",

		"x   "
		"x   "
		"xxx "
		"x  x"
		"x  x"
		"xxx ",

		"    "
		" xx "
		"x  x"
		"x   "
		"x  x"
		" xx ",

		"   x"
		"   x"
		" xxx"
		"x  x"
		"x  x"
		" xxx",

		"    "
		" xx "
		"x  x"
		"xxx "
		"x   "
		" xx ",

		"  xx"
		" x  "
		"xxx "
		" x  "
		" x  "
		" x  ",

		"    "
		" xxx"
		"x  x"
		" xxx"
		"   x"
		"xxx ",
			/////////////
		"x   "
		"x   "
		"xxx "
		"x  x"
		"x  x"
		"x  x",

		" x  "
		"    "
		" x  "
		" x  "
		" x  "
		" x  ",

		"  x "
		"    "
		"  x "
		"  x "
		"x x "
		" x  ",

		"x   "
		"x   "
		"x x "
		"xx  "
		"x x "
		"x x ",

		" x  "
		" x  "
		" x  "
		" x  "
		" x  "
		"  x ",

		"    "
		"x  x"
		"xxxx"
		"xxxx"
		"x  x"
		"x  x",

		"    "
		"xxx "
		"x  x"
		"x  x"
		"x  x"
		"x  x",

		"    "
		" xx "
		"x  x"
		"x  x"
		"x  x"
		" xx ",
			/////////////
		"    "
		"xxx "
		"x  x"
		"x  x"
		"xxx "
		"x   ",

		"    "
		" xxx"
		"x  x"
		"x  x"
		" xxx"
		"   x",

		"    "
		"x xx"
		"xx x"
		"x   "
		"x   "
		"x   ",

		"    "
		" xxx"
		"x   "
		" xx "
		"   x"
		"xxx ",

		"x   "
		"x   "
		"xxx "
		"x   "
		"x   "
		" xx ",

		"    "
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		" xxx",

		"    "
		"x  x"
		"x  x"
		"x  x"
		"x x "
		"xx  ",

		"    "
		"x  x"
		"x  x"
		"xxxx"
		"xxxx"
		"x  x",
			/////////////
		"    "
		"x x "
		"x x "
		" x  "
		"x x "
		"x x ",

		"    "
		"x x "
		"x x "
		"xxx "
		" x  "
		" x  ",

		"    "
		"xxxx"
		"   x"
		" xx "
		"x   "
		"xxxx",

		" xx "
		" x  "
		"xx  "
		" x  "
		" x  "
		" xx ",

		" x  "
		" x  "
		" x  "
		" x  "
		" x  "
		" x  ",

		" xx "
		"  x "
		"  xx"
		"  x "
		"  x "
		" xx ",

		"    "
		"    "
		" x x"
		"x x "
		"    "
		"    ",

		"    "
		" xx "
		" xx "
		" xx "
		"xxxx"
		"xxxx",

	};

	vector<string> asciiBetter = {

		"    "
		"    "
		"    "
		"    "
		"    "
		"    "
		"    "
		"    ",

		" "
		"x"
		"x"
		"x"
		"x"
		" "
		"x"
		" ",

		"   "
		"x x"
		"x x"
		"   "
		"   "
		"   "
		"   "
		"   ",

		"     "
		" x x "
		"xxxxx"
		" x x "
		" x x "
		"xxxxx"
		" x x "
		"     ",

		"     "
		"  x  "
		"xxxxx"
		" xx  "
		"  xx "
		"xxxxx"
		"  x  "
		"     ",

		"   "
		"x x"
		"  x"
		" x "
		" x "
		"x  "
		"x x"
		"   ",

		"     "
		" x   "
		"x x  "
		"xx  x" 
		" xxx "
		"x  xx"
		" xxx "
		"     ",

		" "
		"x"
		"x"
		" "
		" "
		" "
		" "
		" ",
					////////////////////
		" x"
		"x "
		"x "
		"x "
		"x "
		"x "
		"x "
		" x",

		"x "
		" x"
		" x"
		" x"
		" x"
		" x"
		" x"
		"x ",

		"   "
		"   "
		"x x"
		" x "
		"x x"
		"   "
		"   "
		"   ",

		"   "
		"   "
		"   "
		" x "
		"xxx"
		" x "
		"   "
		"   ",

		" " 
		" " 
		" "
		" "
		" "
		" "
		"x"
		"x",

		"   "
		"   "
		"   "
		"   "
		"xxx"
		"   "
		"   "
		"   ",

		" "
		" "
		" "
		" "
		" "
		" "
		"x"
		" ",

		"   x"  
		"   x"  
		"  x "
		"  x "
		" x  "
		" x  "
		"x   "
		"x   ",
					//////////////////////
		"    "
		" xx "
		"x xx"
		"x xx"
		"xx x"
		"xx x"
		" xx "
		"    ",

		"   "
		" x " 
		"xx "
		" x "
		" x "
		" x "
		"xxx"
		"   ",

		"    "
		" xx "
		"x  x"
		"   x"
		"  x "
		" x  "
		"xxxx"
		"    ",

		"    "
		" xx "
		"x  x"
		"  x "
		"   x"
		"x  x"
		" xx "
		"    ",

		"    "
		"x  x"
		"x  x"
		"xxxx"
		"   x"
		"   x"
		"   x"
		"    ",

		"    "
		"xxxx"
		"x   "
		"xxx "
		"   x"
		"   x"
		"xxx "
		"    ",

		"    "
		" xxx"
		"x   "
		"xxx "
		"x  x"
		"x  x"
		" xx "
		"    ",

		"    "
		"xxxx"
		"   x"
		"   x"
		"   x"
		"   x"
		"   x"
		"    ",
					//////////////////////////////////
		"    "
		" xx "
		"x  x"
		" xx "
		"x  x"
		"x  x"
		" xx "
		"    ",

		"    "
		" xx "
		"x  x"
		" xxx"
		"   x"
		"   x"
		"   x"
		"    ",

		" "
		" "
		" "
		"x"
		" "
		"x"
		" "
		" ",

		" "
		" "
		" "
		"x"
		" "
		"x"
		"x"
		" ",

		"   "
		"   "
		"  x"
		" x "
		"x  "
		" x "
		"  x"
		"   ",

		"   "
		"   "
		"   "
		"xxx"
		"   "
		"xxx"
		"   "
		"   ",

		"   "
		"   "
		"x  "
		" x "
		"  x"
		" x "
		"x  "
		"   ",

		"    "
		"xxx "
		"   x"
		" xx "
		" x  "
		"    "
		" x  "
		"    ",
				////////////////////////////////
		"     "
		"     "
		" xxx "
		"x  xx"
		"x  xx"
		"x    "
		" xxxx"
		"     ",

		"    "
		" xx "
		"x  x"
		"x  x"
		"xxxx"
		"x  x"
		"x  x"
		"    ",

		"    "
		"xxx "
		"x  x"
		"xxx "
		"x  x"
		"x  x"
		"xxx "
		"    ",

		"    "
		" xx "
		"x  x"
		"x   "
		"x   "
		"x  x"
		" xx "
		"    ",

		"    "
		"xxx "
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		"xxx "
		"    ",

		"    "
		"xxxx"
		"x   "
		"xxx "
		"x   "
		"x   "
		"xxxx"
		"    ",

		"    "
		"xxxx"
		"x   "
		"xxx "
		"x   "
		"x   "
		"x   "
		"    ",

		"    "
		" xx "
		"x  x"
		"x   "
		"x xx"
		"x  x"
		" xx "
		"    ",
					/////////////////////////////////////
		
		"    "
		"x  x"
		"x  x"
		"xxxx"
		"x  x"
		"x  x"
		"x  x"
		"    ",
		
		"   "
		"xxx"
		" x "
		" x "
		" x "
		" x "
		"xxx"
		"   ",

		"    "
		"   x"
		"   x"
		"   x"
		"   x"
		"x  x"
		" xx "
		"    ",

		"    "
		"x  x"
		"x x "
		"xx  "
		"xx  "
		"x x "
		"x  x"
		"    ",

		"    "
		"x   "
		"x   "
		"x   "
		"x   "
		"x   "
		"xxxx"
		"    ",

		"     "
		"x   x"
		"xx xx"
		"x x x"
		"x   x"
		"x   x"
		"x   x"
		"     ",

		"     "
		"x   x"
		"xx  x"
		"x x x"
		"x x x"
		"x  xx"
		"x   x"
		"     ",

		"    "
		" xx "
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		" xx "
		"    ",
						/////////////
		"    "
		"xxx "
		"x  x"
		"x  x"
		"xxx "
		"x   "
		"x   "
		"    ",

		"    "
		" xx "
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		" xx "
		"   x",

		"    "
		"xxx "
		"x  x"
		"x  x"
		"xxx "
		"x x "
		"x  x"
		"    ",

		"    "
		" xx "
		"x  x"
		" x  "
		"  x "
		"x  x"
		" xx "
		"    ",

		"   "
		"xxx"
		" x "
		" x "
		" x "
		" x "
		" x "
		"   ",

		"    "
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		" xx "
		"    ",

		"     "
		"x   x"
		"x   x"
		"x   x"
		"x   x"
		" x x "
		"  x  "
		"     ",

		"     "
		"x   x"
		"x   x"
		"x   x"
		"x x x"
		"xx xx"
		"x   x"
		"     ",
					/////////////
		"    "
		"x  x"
		"x  x"
		" xx "
		" xx "
		"x  x"
		"x  x"
		"    ",

		"     "
		"x   x"
		"x   x"
		" x x "
		"  x  "
		"  x  "
		"  x  "
		"     ",

		"    "
		"xxxx"
		"   x"
		"  x "
		" x  "
		"x   "
		"xxxx"
		"    ",

		"xx"
		"x "
		"x "
		"x "
		"x "
		"x "
		"x "
		"xx",

		"x   "
		"x   "
		" x  "
		" x  "
		"  x "
		"  x "
		"   x"
		"   x",

		"xx"
		" x"
		" x"
		" x"
		" x"
		" x"
		" x"
		"xx",

		"   "
		" x "
		"x x"
		"x x"
		"   "
		"   "
		"   "
		"   ",

		"    "
		"    "
		"    "
		"    "
		"    "
		"    "
		"xxxx"
		"    ",
						/////////////
		"  "
		"x "
		" x"
		"  "
		"  "
		"  "
		"  "
		"  ",

		"    "
		"    "
		"xxx "
		"   x"
		"xxxx"
		"x  x"
		" xxx"
		"    ",

		"    "
		"x   "
		"x   "
		"xxx "
		"x  x"
		"x  x"
		"xxx "
		"    ",

		"    "
		"    "
		" xx "
		"x  x"
		"x   "
		"x  x"
		" xx "
		"    ",

		"    "
		"   x"
		"   x"
		" xxx"
		"x  x"
		"x  x"
		" xxx"
		"    ",

		"    "
		"    "
		" xx "
		"x  x"
		"xxx "
		"x   "
		" xx "
		"    ",

		"    "
		"  xx"
		" x  "
		"xxx "
		" x  "
		" x  "
		" x  "
		"    ",

		"    "
		"    "
		" xxx"
		"x  x"
		"x  x"
		" xxx"
		"   x"
		"xxx ",
						/////////////
		"    "				
		"x   "
		"x   "
		"xxx "
		"x  x"
		"x  x"
		"x  x"
		"    ",

		" "
		"x"
		" "
		"x"
		"x"
		"x"
		"x"
		" ",

		"   "
		"  x"
		"   "
		"  x"
		"  x"
		"  x"
		"x x"
		" x ",

		"   "
		"x  "
		"x  "
		"x x"
		"xx "
		"x x"
		"x x"
		"   ",

		"  "
		"x "
		"x "
		"x "
		"x "
		"x "
		" x"
		"  ",

		"     "
		"     "
		"xxxx "
		"x x x"
		"x x x"
		"x x x"
		"x x x"
		"     ",

		"    "
		"    "
		"xxx "
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		"    ",

		"    "
		"    "
		" xx "
		"x  x"
		"x  x"
		"x  x"
		" xx "
		"    ",
					/////////////
		"    "
		"    "
		"xxx "
		"x  x"
		"x  x"
		"xxx "
		"x   "
		"x   ",

		"    "
		"    "
		" xxx"
		"x  x"
		"x  x"
		" xxx"
		"   x"
		"   x",

		"    "
		"    "
		"x xx"
		"xx x"
		"x   "
		"x   "
		"x   "
		"    ",

		"    "
		"    "
		" xxx"
		"x   "
		" xx "
		"   x"
		"xxx "
		"    ",

		"   "
		"x  "
		"x  "
		"xxx"
		"x  "
		"x  "
		" xx"
		"   ",

		"    "
		"    "
		"x  x"
		"x  x"
		"x  x"
		"x  x"
		" xxx"
		"    ",

		"    "
		"    "
		"x  x"
		"x  x"
		"x  x"
		"x x "
		"xx  "
		"    ",

		"     "
		"     "
		"x   x"
		"x   x"
		"x x x"
		"x x x"
		" xxxx"
		"     ",
					/////////////
		"   "
		"   "
		"x x"
		"x x"
		" x "
		"x x"
		"x x"
		"   ",

		"    "
		"    "
		"x  x"
		"x  x"
		"x  x"
		" xxx"
		"   x"
		"xxx ",

		"    "
		"    "
		"xxxx"
		"   x"
		" xx "
		"x   "
		"xxxx"
		"    ",

		" xx"
		" x "
		" x "
		"xx "
		"xx "
		" x "
		" x "
		" xx",
		
		"x"
		"x"
		"x"
		"x"
		"x"
		"x"
		"x"
		"x",


		"xx "
		" x "
		" x "
		" xx"
		" xx"
		" x "
		" x "
		"xx ",

		"    "
		"    "
		"    "
		" x x"
		"x x "
		"    "
		"    "
		"    ",

		"    "
		"    "
		"xxxx"
		"x  x"
		"x  x"
		"xxxx"
		"    "
		"    ",

	};

	vector<string> glyphs = {
		" ", "🬀", "🬁", "🬂", "🬃", "🬄", "🬅", "🬆",
		"🬇", "🬈", "🬉", "🬊", "🬋", "🬌", "🬍", "🬎",
		"🬏", "🬐", "🬑", "🬒", "🬓", "▌", "🬔", "🬕",
		"🬖", "🬗", "🬘", "🬙", "🬚", "🬛", "🬜", "🬝",
		"🬞", "🬟", "🬠", "🬡", "🬢", "🬣", "🬤", "🬥",
		"🬦", "🬧", "▐", "🬨", "🬩", "🬪", "🬫", "🬬",
		"🬭", "🬮", "🬯", "🬰", "🬱", "🬲", "🬳", "🬴",
		"🬵", "🬶", "🬷", "🬸", "🬹", "🬺", "🬻", "█"
	};


	// string getCorner(char c, bool down, bool right){
	// 	if(c < ' ' || c > '~') c = '?';
	// 	string s = asciiClassic_4x6[c-32];
	// 	int h = 2*right+12*down;
	// 	int val = (s[h+0]=='x')   + (s[h+1]=='x')*2 
	// 			+ (s[h+4]=='x')*4 + (s[h+5]=='x')*8
	// 			+ (s[h+8]=='x')*16+ (s[h+9]=='x')*32;
	// 	return glyphs[val];
	// }


	// vector<string> getLetter(char c){
	// 	if(c < ' ' || c > '~') c = '?';
	// 	return {
	// 		getCorner(c, 0, 0),
	// 		getCorner(c, 0, 1),
	// 		getCorner(c, 1, 0),
	// 		getCorner(c, 1, 1)
	// 	};
	// }


	vector<string> generate(string s, int width){
		width*=2;
		vector<vector<bool>> bitmap;
		bitmap.push_back(vector<bool>(width, 0));
		bitmap.push_back(vector<bool>(width, 0));

		int currentLine = 2;
		for(int i=0;i<8;i++)
			bitmap.push_back({});

		for(auto x:s){
			if(x<' '||x>'~') x = '~'+1;
			string schema = asciiBetter[x-32];
			// string schema = asciiBetter[14];
			int letterWidth = schema.size()/8;
			if(bitmap[currentLine].size() + letterWidth > width){
				while(bitmap[currentLine].size() < width){
					for(int i=0;i<8;i++)
						bitmap[currentLine+i].push_back(0);
				}
				bitmap.push_back(vector<bool>(width, 0));
				for(int i=0;i<8;i++)
					bitmap.push_back({});
				currentLine+=9;
			}
			for(int i=0;i<schema.size();i++)
				bitmap[currentLine + i/letterWidth].push_back(schema[i]=='x');
			for(int i=0;i<8;i++)
				bitmap[currentLine+i].push_back(0);
		}
		while(bitmap[currentLine].size() < width){
			for(int i=0;i<8;i++)
				bitmap[currentLine+i].push_back(0);
		}

		bitmap.push_back(vector<bool>(width, 0));
		bitmap.push_back(vector<bool>(width, 0));

		vector<string> res;


		for(currentLine=0; currentLine< bitmap.size(); currentLine+=3){
			res.push_back("");
			for(int i=0;i<width;i+=2){
				int sum = bitmap[currentLine  ][i]*1 + bitmap[currentLine ][i+1]*2
						+ bitmap[currentLine+1][i]*4 + bitmap[currentLine+1][i+1]*8
						+ bitmap[currentLine+2][i]*16+ bitmap[currentLine+2][i+1]*32;
				res.back() += glyphs[sum];
			}
		}

		// for(auto x:res) cout<<x<<endl;

		return res;

	}

}


// int main(){

// 	bigLetter::generate("e^pi + 1 < 0", 50);
// 	return 0;

// }

	// class bigString{
	//     public:

	//     string upper, lower;

	//     bigString(){
	//         upper = lower = "";
	//     }

	

	//     void add(char c){
	//         upper += corner(c, 0, 0) + corner(c, 0, 1);
	//         lower += corner(c, 1, 0) + corner(c, 1, 1);
	//     }


	//     void add(string s){
	//     for(auto x:s) add(x), upper+=" ", lower+=" ";
	//     }

	//     void print(){
	//         cout<<upper<<"\n";
	//         cout<<lower<<'\n';
	//     }
	// };


	// int main(){

	//     // const char x[4] = "\u2800";

	//     // char y[4];
	//     // for(int i=0;i<4;i++) y[i]=x[i];
	//     // cout<<x;
	//     // for(int i=0;i<64;i++)
	//     //     cout<<glyphs[i];
	//     cout<<(int)'|'<<" "<<(int)'}'<<", "<<ascii4x6.size()+32<<endl;
	//     bigString bs;   
	//     bs.add("{Satori} Enchants<int>!?");
	//     bs.print();
		
	//     // // cout<< ascii4x6[0];
	//     // // printf("%lc", 2800);
	//     // // cout << "\u2800";

	//     // cout<<"🬨🬨🬨🬨";

	// }



