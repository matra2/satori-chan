#pragma once
#include "view.hpp"
// #include "printer.cpp"
#include "pageBuilder/all.cpp"
#ifndef PARSER_GUARD
    #include "../html/parser.cpp"
#endif


view::view(){
    // printHandler = new printer(this);
}




void view::fetchFromWeb(string name){
    string cookieFile = "\"" + RUNTIME_DIR + "/cookie.txt" + "\"";

	string argPref = "curl -b " + cookieFile + " -c  " + cookieFile;
    string argPost = " \"" + state.url + "\" -o \"" + name +  "\" -L -w %{url_effective}";
    string argIn = "";
    for(auto x:state.formValues) argIn += " -d \"" + x.st + "=" + x.nd + "\" ";
// 
    string arg = argPref + argIn + argPost;
    
    cout<<"[view::fetchFromWeb]:\tDOING CURL..."<<endl;
    //cout<<"CURL URL: " << arg<<endl;
    LOG("outside", arg);
    system((arg + " > " + RUNTIME_DIR + "/link.txt" + " 2>> " + DEBUG_DIR+ "/outside").c_str());
    LOG("outside", "\n");
    cout<<"[view::fetchFromWeb]:\t... CURL DONE"<<endl;

    ifstream in;
    in.open(RUNTIME_DIR + "/link.txt");
    getline(in, state.url);

}

void view::openPage(string _url, bool force){

    if(history.size()==0 || history.back() != _url)
        history.push_back(_url);

    cout<<"[view::openPage]:\tOPPENING..."<<endl;
    LOG.clear();
    state.url = _url;
    if(state.url.substr(0,28) == "https://satori.tcs.uj.edu.pl")
        state.satoriDomain = 1;
    else 
        state.satoriDomain = 0;
    
    string filename = state.url + ".html";
    for(auto&x:filename) if(x == '/') x = '_';
    filename = CACHE_DIR + filename;
    cout<<"[view::openPage]:\tFILENAME: " + filename<<endl;

    state.cached = 1;
    ifstream in;
    in.open(filename);
    if(force || !in.good()){
        fetchFromWeb(filename);
        state.cached = 0;
        cout<<"[view::openPage]:\tEFFECTIVE URL: " + state.url<<endl;
    }
    else   
        cout << "[view::openPage]:\tFile in cache, skipping download"<<endl;
    in.close();


    root = htmlAst::all(filename);
    if(state.satoriDomain) 
        satoriActivities(state, root);

    state.formValues.clear();
    state.links.clear();
    state.data.clear();
    
    PrintPage();

}



void view::openPageLocal(string _name){


    cout<<"[view::openPageLocal]:\tOPPENING..."<<endl;
    LOG.clear();
    state.url = "NULL";
    state.satoriDomain = 1;

    root = htmlAst::all(_name);

    state.formValues.clear();
    state.links.clear();
    state.data.clear();

    PrintPage();

}






void view::PrintPage(){

    cout << "[view::PrintPage]:\t Printing..."<<endl;
    //system("clear");

    // auto res = printHandler->printPage(root);
    // pageState state;
    cout<<"[viwe::PrintPage]:\t"<<"Printing..."<<endl;
    state.links.clear();
    auto res = pageBuilder::utils::printPage(&state, root);

    for(auto x:res)
        cout<<x<<endl;

}





void view::satoriActivities(pageState state, astNode* node){
    cout << "[viwe::satoriActivities]:\t" << "Acting..."<<endl;
    using namespace pageBuilder::utils;
    //string s = "https://satori.tcs.uj.edu.pl/contest/setect";
    //cout << "myurl: " <<state.url<<";"<<endl;
    //cout << "targt: " <<s<<";"<<endl;
    //cout << (state.url == s)<<endl;
    //cout<<"siz: "<<state.url.size() << ", " << s.size()<<endl;
    //for(int i=0;i<s.size();i++){
        //cout <<"s: "<<s[i]<< ", u: "<<state.url[i]<<"\t res:" << (s[i]==state.url[i])<<endl;
        //}


    if(state.url == (string)"https://satori.tcs.uj.edu.pl/contest/select"){

        cout << "[viwe::satoriActivities]:\t" << "Contest act..."<<endl;
        auto table = findTag(root, "table", {{"class", "results"}}); 
        if(!table){
            cout <<"ERROR: no table"<<endl;
            return;
        }
        ofstream out;
        out.open(RUNTIME_DIR + "/contestList.html");


        bool first = 1;
        for(auto tr:table->ch){
            if( first){ first = 0; continue;}
            out << "<li> <a href=\"" << tr->ch[0]->ch[0]->args["href"] << "problems\">";
            out << accRaw(&state, tr->ch[0]->ch[0], 20).toString();
            out << "</a> </li>"<<endl;
        }


    }

    if(state.url.size() > 9 && state.url.substr(state.url.size()-9, 9) == "/problems"){

        // cout<<"PROBLEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEMS"<<endl;

        string filename = state.url;
        for(auto&x:filename) if(x == '/') x = '_';

        {
            ifstream results;
            results.open(RESULTS_DIR + filename);
            if(!results.good()) return;
            results.close();
        }

        astNode* results = htmlAst::all(RESULTS_DIR  + filename);

        map<string, string> resMap;

        for(auto x: results->ch[0]->ch){
            // cout<<x->args["contest"]<<": "<<x->args["result"]<<endl;
            resMap[x->args["contest"]] = x->args["result"];
        }


        auto table = pageBuilder::utils::findAllTags(node, "table", {{"class", "results"}});

        for(auto t:table) for(int i=1;i<t->ch.size(); i++){
            string problem = t->ch[i]->ch[0]->ch[0]->in.toString();
            if(resMap[problem] == "OK")
                t->ch[i]->ch[0]->args["class"] = "staOK";
            else if(resMap[problem] != "")
                t->ch[i]->ch[0]->args["class"] = "staANS";
        }



    }


}





void scrape(string url){
    
    view v;
    // v.fetchFromWeb( url.substr(0, url.size()-9) + "/results?results_limit=1000&results_page=1");

    v.openPage( url.substr(0, url.size()-9) + "/results?results_limit=1000&results_page=1", 1);

    auto n = v.root;
    auto table = pageBuilder::utils::findTag(n, "table", {{"class", "results"}});
        
    map<string, string> results;

    for (int i=1;i<table->ch.size();i++){
        auto& x = table->ch[i];
         
        string prob = x->ch[1]->ch[0]->in.toString();
        string res = x->ch[3]->ch[0]->ch[0]->ch[0]->in.toString();
        
        if(results[prob] != "OK")
            results[prob] = res;

    }

    ofstream out;
    for(auto&x:url) if(x == '/') x = '_';
    out.open(RESULTS_DIR + url);
    out << "<ul>\n";
    for(auto x: results)
        out << "\t<li contest=\"" + x.first +"\" result=\"" + x.second + "\"> </li>\n";
    out << "</ul>\n";
    out.close();
    for(auto x: results)
        cout <<x.first << ": "<<x.second << endl;
    
}









bool view::handleInput(){

    cout<<"\nEnter command $ ";

    string input;
    cin >> input;



    if(input == "exit" || input == "e") 
        return 0;


    if(input == "help" || input == "h"){
        cout << "unimplemented!();\n";
        return 1;
    }

    if(input == "open" || input == "o"){
        cin >> input;
        openPage(input);
        return 1;
    }

    if(input == "reload" || input == "r"){
        openPage(state.url, 1);
        return 1;
    }

    if(input == "back" || input == "b"){
        history.pop_back();
        if(history.size()==0){
            cout<<"No previous page.\n";
            return 1;
        }
        state.url = history.back();
        openPage(state.url);
        return 1;
    }

    if(input == "scrape"){
        if(state.url.substr(state.url.size()-9, 9) != "/problems"){
            cout << "not problem site\n";
            return 1;

        }
        scrape(state.url);
        openPage(state.url);
        return 1;

    }


    int i;
    try{
        i = stoi(input);
    }catch(exception e){
        cout << "INVALID INPUT\n";
        return 1;
    }


    if(i<0 || i>=state.links.size()){
        cout << "INPUT TO BIG"<<endl;
        return 1;
    }

    string nameLink = state.links[i].nd->name;
    string typeLink = state.links[i].nd->args["type"];

    astNode* node = state.links[i].nd;

    cout<<"name: "<<nameLink <<endl<<"type: "<<typeLink<<endl;

    if(nameLink == "input" && state.links[i].nd->args["class"] == "button"){
        openPage(state.url, 1);
        return 1;
    }

    if(nameLink == "input" && (typeLink == "text" || typeLink == "password" || typeLink == "file")){
        cin >> input;
        state.formValues[state.links[i].first] = input;
        PrintPage();
        return 1;
    }

    if(nameLink == "select"){
        state.data[state.links[i].first] = 1;
        PrintPage();
        return 1;
    }

    if(nameLink == "option"){
        string parentName = state.links[node->optiones["selectParent"]].st;
        state.data[parentName] = 0;
        node->args["selected"] = "selected";
        PrintPage();
        return 1;
    }

        

    if(nameLink == "pre"){
        cout<<"--------------------------"<<endl;
        cout<<state.links[i].st<<endl;
        cout<<"--------------------------"<<endl;
        return 1;
    }

 
    if(nameLink == "img"){
        string filename = state.links[i].st;
        for(auto&x:filename) if(x == '/') x = '_';
        filename = CACHE_DIR + filename;
        fetch("https://satori.tcs.uj.edu.pl" + state.links[i].st, filename);

        terminal("feh "+ filename + " &");
        return 1;
    }

    string extension = getExtension(state.links[i].st);

    if(extension == "?"){
        openPage("https://satori.tcs.uj.edu.pl" + state.links[i].st);
        return 1;
    }
    if(extension == "http"){
        openPage(state.links[i].st);
        return 1;
    }

    if(extension == "html"){
        cout << "Going outside of satori\n";
        openPage(state.links[i].st);
        return 1;
    }

    string filename = state.links[i].st;
    for(auto&x:filename) if(x == '/') x = '_';
    filename = CACHE_DIR+ filename;
    fetch("https://satori.tcs.uj.edu.pl" + state.links[i].st, filename);
    string program = extApp[extension];
    if(program == "") program  = extApp["other"];
    cout << "Downloaded to " + filename << "\nOpen with " + program + "? (y/n) $ ";
    char c;
    cin >> c;

    if(c!='n'){
        string program = extApp[extension];
        if(program == "") program  = extApp["other"];
        terminal(program + " " + filename);
    }

    return 1;

}
