// #include <bits/stdc++.h>
#pragma once
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>

using namespace std;

#define DEFINES_GUARD 1


#define BOLD    (string)"\033[1m"
// #define THIN    (string)"\033[22m"
// #define ITAL    (string)"\033[3m"
// #define UNDER   (string)"\033[4m"

// #define WHITE   (string)"\033[37m"
#define RED     (string)"\033[31m"
// #define GREEN   (string)"\033[32m"
// #define YELLOW  (string)"\033[33m"

// #define BWHITE   (string)"\033[1;37m"
// #define BRED     (string)"\033[1;31m"
// #define BGREEN   (string)"\033[1;32m"
// #define BYELLOW  (string)"\033[1;33m"

#define NORMAL  (string)"\033[0m"

// #define BLACK   string("\033[90m")
// #define RED     string("\033[91m")
// #define GREEN   string("\033[92m")
// #define YELLOW  string("\033[93m")
// #define BLUE    string("\033[94m")
// #define PURPLE  string("\033[95m")
// #define CYAN    string("\033[96m")
// #define WHITE   string("\033[97m")

// #define DBLACK  string("\033[30m")
// #define DRED    string("\033[31m")
// #define DGREEN  string("\033[32m")
// #define DYELLOW string("\033[33m")
// #define DBLUE   string("\033[34m")
// #define DPURPLE string("\033[35m")
// #define DCYAN   string("\033[36m")
// #define DWHITE  string("\033[37m")


#define pb push_back
#define st first
#define nd second

#define ENT token("<ENTER>", 1)
#define SPC token("<SPACE>", 1)
#define NL  token("<ENDL>", 1)
#define RAW  token("<RAW>", 1)
#define BLOCK  token("<BLOCK>", 1)
#define NON  token("<NULL>", 1)
#define TAB  "    "


#include "rootDir.h"

const string DEBUG_DIR = ROOT_DIR + "/logs/";
const string CACHE_DIR = ROOT_DIR + "/files/cache/";
const string RUNTIME_DIR = ROOT_DIR + "/files/runtime/";
const string TEMPLATES_DIR = ROOT_DIR + "/files/templates/";
const string RESULTS_DIR = ROOT_DIR + "/files/results/";



map<string,string> latexCommand = {
    {"(",""}, {")", ""}, {"[", "["}, {"]", "]"}, {"{", "{"}, {"}", "}"}, 
    {"le", " < "}, {"leq", " <= "}, {"leqslant", " <= "},
    {"cdot", "*"}, {"ldots", "..."}, {"cdots", "..."},
    {",", ""}
}; 


map<string,string> extApp = {
    {"sql", "vim"},
    {"pdf", "firefox"},
    {"png", "feh"},
    {"jpg", "feh"},
    {"cpp", "vim"},
    {"hpp", "vim"},
    {"h", "vim"},
    {"c", "vim"},
    {"txt", "vim"},
    {"other", "firefox"}
};



// vector<string> LOGO = {
//     BOLD + "  +------------------------+",
//            "  |                        |",
//            "  |        L,  L, "+RED+",J"+NORMAL+BOLD+"       |",
//            "  |     ,i__Z___Z_"+RED+"S"+NORMAL+BOLD+"___i    |", 
//            "  |    .J  ,______,  J'    |", 
//            "  |        I______I        |",
//            "  |        I______I        |",
//            "  |        I______I        |", 
//            "  |        \"/   L,\"  i     |", 
//            "  |     __-J\"   \"L___I     |", 
//            "  |                        |", 
//            "  +------------------------+"
// };



string repeat(int i, char c){
    if(i<=0) return "";
    return string(i, c);
}

string repeat(int i, string c){
    if(i<=0) return "";
    string res;
    for(int j=0;j<i;j++)
        res += c;
    return res;
}

// string repeat(int i, string s){
//     if(i<=0) return "";
//     string res;
//     while(i>= s.size()) 
//         res += s, i-=s.size();
//     res += s.substr(0, i);
//     return res;
// }


struct logger{

    vector<ofstream*>ofs;
    vector<string> names;

    logger(){
        system(("rm -rf " + DEBUG_DIR).c_str());
        system(("mkdir " + DEBUG_DIR).c_str());
    }


    void openNew(string name){
        names.pb(name);
        ofstream*o = new ofstream();
        o->open(DEBUG_DIR+name);
        ofs.pb(o);
    }  


    void operator()(string name, string mes){
        //for(int i=0;i<ofs.size();i++) if(names[i]==name){
            //(*ofs[i])<<mes<<endl;
            //return;
        //}
        //openNew(name);
        //(*ofs.back())<<mes<<endl;
        

        ofstream of;
        of.open(DEBUG_DIR+name, std::ios_base::app);
        of << mes << "\n";
        of.close();

     //   system(("echo " + mes + " >> " + ROOTDIR + "/debug/" + name).c_str());
    }

    void clear(string name){
        // openNew(name);
    }

    void clear(){
        // system("rm -rf debug");
        // system("mkdir debug");
        // for(int i=0;i<ofs.size();i++){
        //     ofs[i]->close();
        //     ofs[i]->open(names[i]);
        // }
    }

    ~logger(){

    }

} LOG;


void fetch(string link, string name){
    // LOG("fetch", "START");
	string arg = "curl -b  cookie.txt  \"" + link + "\" -o " + name + " 2>> " + DEBUG_DIR + "outside -L";
    LOG("outside", arg);
	system(arg.c_str());

}

string getExtension(string s){

    int i;
    for(i=s.size()-1; i>=0;i--) if(s[i]=='.') break;

    if(i<0) return "?";
    if(s.size() > 4 && s.substr(0, 4) == "http") return "http";

    string ext;
    i++;
    while(i<s.size() && s[i]!='#') ext += s[i++];

    return ext;

}


void terminal(string s){
    system((s + " 2>> " + DEBUG_DIR + "outside").c_str());
    LOG("outside", "\n");
}



// vector<vector<string>> tableBorder = {
//     {".-", "-", "-.-", "-."},
//     {"| ", " ", " | ", " |"},
//     {"|-", "-", "-+-", "-|"},
//     {"'-","-", "-'-", "-'"}
// };

// vector<vector<string>> tableBorder = {
//     {"/-", "-", "-v-", "-\\"},
//     {"| ", " ", " | ", " |"},
//     {">-", "-", "-+-", "-<"},
//     {"\\-","-", "-^-", "-/"}
// };

// vector<vector<string>> pageBorder = {
//     {"█▀", "▀", "▀▀▀", "▀█"},
//     {"█ ", " ", " █ ", " █"},
//     {"█▄", "▄", "▄▄▄", "▄█"},
//     {"█▄", "▄", "▄█▄", "▄█"}
// };

// vector<vector<string>> pageBorder = {
//     {"🬚🬋", "🬋", "🬋🬋🬋", "🬋🬓"},
//     {"▌ ", " ", " ▌ ", " ▌"},
//     {"🬛🬋", "🬋", "🬋🬚🬋", "🬋▌"},
//     {"🬌🬋", "🬋", "🬋🬌🬋", "🬋🬄"}
// };
vector<vector<string>> pageBorder = {
    {"🬹🬋", "🬋", "🬋🬋🬋", "🬩🬓"},
    {"▌ ", " ", " ▌ ", " ▌"},
    {"█🬋", "🬋", "🬩🬹🬋", "🬫▌"},
    {"🬎🬋", "🬋", "🬍🬎🬋", "🬍🬄"}
};

// vector<vector<string>> pageBorder = {
//     {"┏━", "━", "━━━", "━┓"},
//     {"┃ ", " ", " ┃ ", " ┃"},
//     {"┣━", "━", "━┳━", "━┫"},
//     {"┗━", "━", "━┻━", "━┛"}
// };
vector<vector<string>> preBorder = {
    {"┌─", "─", "─┬─", "─┐"},
    {"│ ", " ", " │ ", " │"},
    {"├─", "─", "─┼─", "─┤"},
    {"└─", "─", "─┴─", "─┘"}
};
// vector<vector<string>> tableBorder = {
//     {"┏━", "━", "━┯━", "━┓"},
//     {"┃ ", " ", " │ ", " ┃"},
//     {"┠─", "─", "─┼─", "─┨"},
//     {"┗━", "━", "━┷━", "━┛"}
// };
vector<vector<string>> tableBorder = {
    {"╔═", "═", "═╤═", "═╗"},
    {"║ ", " ", " │ ", " ║"},
    {"╟─", "─", "─┼─", "─╢"},
    {"╚═", "═", "═╧═", "═╝"}
};




//▀▄▌
//━ ┳  ┻ ┫ ┣ 
 
// vector<vector<string>> pageBorder = {
//     {"#=", "=", "===", "=#"},
//     {"[ ", " ", " I ", " ]"},
//     {"#=", "=", "=#=", "=#"},
//     {"#=", "=", "=#=", "=#"}
// };

// vector<vector<string>> gridBorder = {
//     {"===", " I "},
//     {" ^=", "=^ ", "=V ", " V="},
//     {" I=", "=^=", "=I ", "=V="},
//     {"=#="}
// };

// vector<vector<string>> pageBorder = {
//     {"$=", "~", "-v-", "=@"},
//     {"J ", " ", " i ", " I"},
//     {">-", ".", "-+-", "-<"},
//     {"!=","~", "-^-", "=#"}
// };
