#pragma once
#include "../defines/defines.hpp"
#include "codefrag.hpp"
#include "token.cpp"


codefrag::codefrag(){
    tab = {};
}

codefrag::codefrag(char*t){
    tab = {{t}};
}

codefrag::codefrag(string t){
    tab = {{t}};
}

codefrag::codefrag(token t){
    tab = {t};
}

codefrag::codefrag(vector<token> v){
    tab=v;
}

int codefrag::size(){
    return tab.size();
}

int codefrag::displaySize(){
    int res = 0, acc = 0;
    for(auto x:tab){
        if(x==NL) res = max(res, acc), acc=0;
        else acc += x.displaySize();
    } 
    res = max(res, acc), acc=0;
    return res;
}


// void codefrag::parse(string s){
//     tab = {};
//     for(int i=0;i<s.size();i++){

//         string t="";

//         // int sp=0;
//         // while(i<s.size() && s[i]==' ' ) sp++,i++;
//         // if(sp>0){
//         //     tab.pb({"<SPACE> "+to_string(sp),1});
//         //     continue;
//         // }
//         // cout<<"sp: "<<sp<<endl;

//         if(s[i]==' '){ 
//             int rep=1;
//             while(s[i+1]==' ') i++, rep++;
//             tab.pb(SPC); 
//             tab.back().rep=rep;
//             continue;
//         }

//         if(s[i]=='\t') continue;

//         if(s[i]=='\n'){
//             int rep=1;
//             int lastEnter=i;
//             while(isspace(s[i+1])){
//                 i++;
//                 if(s[i]=='\n')lastEnter=i, rep++;
//             }
//             i=lastEnter;
//             tab.pb(ENT); 
//             tab.back().rep=rep;
//             continue;
//         }

//         if(s[i]=='<'){
//             while(i<s.size()&&s[i]!='>') t+=s[i], i++;
//             t+='>';
//         }
//         else{
//             while(i<s.size() && s[i]!='<' && s[i]!=' ' && s[i]!='\n' && s[i]!='\t')
//                 t+=s[i], i++;
//             i--;
//         }

//         tab.pb(t);
//     }
   
//     for(auto x:tab) LOG("codeFragParser.txt",x.rep==1 ? x.val : x.val+":"+to_string(x.rep));

// }


token& codefrag::back(){
    if(tab.size()==0) LOG("error", "codefrag:back: index out of bounds");
    return tab[tab.size()-1];
}

void codefrag::pop(){
    if(tab.size()==0) LOG("error", "codefrag:pop: was suposed to pop but was empty");
    tab.pop_back();
}

vector<token>::iterator codefrag::begin(){
    return tab.begin();
}

vector<token>::iterator codefrag::end(){
    return tab.end();
}


codefrag codefrag::operator=(codefrag c){
    tab=c.tab;
    return *this;
}  

string codefrag::toString(){
    string res="";
    for(auto x:tab){
        if(!x.isSpec()) res+=x.toString();//if(x.val!="<SPACE>" && x.val!="<ENTER>")s
        if(x == SPC) res+=" ";
        if(x == NL) res+="\n";
    }
    return res;

} 
string codefrag::toStringRaw(){
    string res="";
    for(auto x:tab) 
        res +=x.toString();
    return res;
} 

token& codefrag::operator[](int i){
    if(i<0 || i>= tab.size()) return tab[tab.size()-1];
    return tab[i];
}


void codefrag::clear(){
    tab.clear();
}


////////////////////////////////// DODAWANIE & OPERATORY


codefrag& operator<<(codefrag&c, token t){
    c.tab.pb(t);
    return c;
}

codefrag operator<<(token l, token r){
    return codefrag({l,r});
}

codefrag& operator<<(codefrag&c, string t){
    c.tab.pb({t});
    return c;
}

codefrag& operator<<(codefrag&c, const char* t){
    c.tab.pb({t});
    return c;
}

codefrag& operator<<(codefrag&l, codefrag r){
    for(auto x:r.tab) l<<x;
    return l;
}

codefrag& operator<<(codefrag&c, material f){
    if(c.size()==0){
        LOG("error", "codefrag:<<: codefrag empty");
        return c;
    }
    c.back().mat = f;
    return c;
}


codefrag operator+(codefrag l, material r){

    for(auto&x:l) x.mat = x.mat + r;
    return l;
}

codefrag& operator+=(codefrag&l, material r){

    for(auto&x:l) x.mat = x.mat + r;
    return l;
}


ostream& operator<<(ostream &out, codefrag &c){
    for(auto x:c) out<<x;
    return out;
}