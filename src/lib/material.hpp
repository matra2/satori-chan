#pragma once

#include <string>
#include <fstream>

#include "termColor.hpp"
using namespace std;


/**
 * @brief list of rules how to print a token to the screen
 * 
 */
struct material{

    public:

            // Characteristics of displayed text 
        bool bold, italic, underline;
        
            // Color of displayed text 
        termColor color;




    public:

            // Default constructor, does not change displayed text
        material();

            // Sets color of text
        material(termColor);

            /**
             * @brief Construct a new material object
             * 
             * @param color the color of text
             * @param mat characteristics of text coded in bits: 
             * 0-bold, 1-italic, 2-underline
             */
        material(termColor color, int mat);



        // chages display properties of the following characters in the given stearm
    friend ostream& operator<<(ostream &out, material mat);

        /**
         * @brief Stacks properties of both material
         * @param l has lower priority
         * @param r has higher priority
         */
    friend material operator+(material l, material r);

  

};

    

