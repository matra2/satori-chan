#include "material.hpp"


material::material(){
    bold=italic=underline=0;
    color=termColor::white;
}

material::material(termColor _color){
    bold=italic=underline=0;
    color=_color;
}


material::material(termColor _color, int mat){
    bold = (mat >> 0) & 1;        //1
    italic = (mat >> 1) & 1;      //2
    underline = (mat >> 2) & 1;   //4

    color=_color;
}

ostream& operator<<(ostream &out, material mat){
    out<<"\033[0m";
    if(mat.bold) out<<"\033[1m";
    if(mat.italic) out<<"\033[3m";
    if(mat.underline) out<<"\033[4m";
    out<<toString(mat.color);
    return out;
}

material operator+(material l, material r){
    material res=r;
    res.bold |= l.bold;
    res.italic |= l.italic;
    res.underline |= l.underline;
    return res;
}