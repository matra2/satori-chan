#pragma once

#include "material.hpp"
#include <string>

// #include "../defines/defines.hpp"


    /**
     * @brief Contains a smallest minigful command in HTML 
     */
struct token{

    public:

            // Raw string
        string val;

            // Material
        material mat;

            // Number of repetitions 
        int rep;


    public:


            /**
             * @brief Construct a new empty token object
             * 
             */
        token();

            /**
             * @brief Construct a new token object
             * 
             * @param s text of token
             */
        token(string s);

            /**
             * @brief Construct a new token object
             * 
             * @param s text of token
             */
        token(char* s);

            /**
             * @brief Construct a new token object
             * 
             * @param s text of token
             * @param f material of token
             */
        token(char* s, material f);

              /**
             * @brief Construct a new token object
             * 
             * @param s text of token
             * @param f material of token
             */
        token(string s, material f);

            /**
             * @brief Construct a new special token object
             * 
             * @param s text of token
             * @param b is ignored
             */
        token(string s, bool);

   
    public:
            // Returns the type of token
        int getType();

            // Is the token special
        bool isSpec();

            // Returns number of chars in memory
        int size();

            // Returns number of chears displayed on screen;
        int displaySize();

            // Returns parameter val
        string toString();

            // Returns i-th char
        char operator[](int i);

            // check if token has prefix s
        bool begins(string s);



    private:

            // 0-text, 1-begining, 2-ending, 3-sigleton, 4-comment.
        int type;

            // If the token represents a special string
        bool spec;



    private:

            //sets the parameter type
        void setType(string s);


        friend ostream& operator<<(ostream &out, token t);
        friend bool operator==(token, token);

};
