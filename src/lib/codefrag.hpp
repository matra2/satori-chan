#pragma once

#include <vector>
#include "token.hpp"
using namespace std;


    /// @brief Reprezents list of tokens
struct codefrag{


public:
        /// @brief Raw vector
    vector<token> tab;
    

public:

        /// @brief Consturcts empty codefrag
    codefrag();

    codefrag(char*t);
    
    codefrag(string t);
    
    codefrag(token t);

    codefrag(vector<token> v);


        /// @brief Returns number of tokens
    int size();

        /// @brief String of all tokens with spaces between
    string toString();

        /// @brief String of all tokens with spaces between also shows special tokens
    string toStringRaw();

        /// @brief Returns size of this code frag as seen in screen
    int displaySize();

        /// @brief Copy constructor
    codefrag operator=(codefrag c);

        /// @brief Returns i-th token
    token& operator[](int i);

        /// @brief Returns last token
    token& back();

        /// @brief pops the last element if exists
    void pop();

        /// @brief Clears 
    void clear();
    
        /// @brief Begin iterator
    vector<token>::iterator begin();

        /// @brief End iterator
    vector<token>::iterator end();


};




codefrag& operator<<(codefrag&c, token t);

codefrag operator<<(token l, token r);

codefrag& operator<<(codefrag&c, string t);

codefrag& operator<<(codefrag&c, const char* t);

codefrag& operator<<(codefrag&l, codefrag r);

codefrag& operator<<(codefrag&c, material f);

codefrag operator+(codefrag l, material r);

codefrag& operator+=(codefrag&l, material r);

ostream& operator<<(ostream &out, codefrag &c);
