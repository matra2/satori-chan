#include "token.hpp"
#include "material.cpp"


token::token() : token(string{""}){
}

token::token(char* s) : token((string)s){
}

token::token(string s){
    val=s;
    spec=0;
    rep=1;
    setType(s);
}

token::token(char* s, material f) : token((string)s, f) {
}

token::token(string s, material f) : token(s){
    mat = f;
}

token::token(string s, bool b){
    val=s, spec=1, type=-1, rep=1;
}




int token::size(){
    return val.size();
}

int token::displaySize(){
    if(spec) return 0;
    int len=0;
    // for(auto x:val) if(!(32>x or 126) && x>0) len++;
    for(int i=0;i<val.size();i++) {
        char x = val[i];
        if(x>0)len++;
        if(x<0){
            if(x& (1<<6)) len++;
            // i-=1;
            // if(x&& (1<<6)) i++;
            // if(x&& (1<<5)) i++;
        }
    }
    // len=val.size() - len/2;
    return len;
}

void token::setType(string s){
    if(s.size()>0 && s[0]!='<'){
        type=0;
        return;
    }
    if(s.size()>1 &&s[1]=='/'){
        type=2;
        return;
    }
    if(s.size()>1 && s.substr(0,2)=="<!"){
        type = 4;
        return;
    }
    if(s.size()>1 &&s[s.size()-2]=='/' || s.size()>3&&s.substr(0,4)=="<img" || s.size()>1&&s[1]=='!'){
        type=3;
        return;
    }
    type=1;
}


bool token::begins(string s){
    if(val.size()<s.size()) return 0;
    return val.substr(0, s.size())==s;
}


char token::operator[](int i){
    if(i<0 or i>=val.size()) return '?';
    return val[i];
}

string token::toString(){
    string res = val;
    return res;
}

int token::getType(){
    return type;
}

bool token::isSpec(){
    return spec;
}





ostream& operator<<(ostream &out, token t){
    if(t.spec) return out;
    out<<t.mat<<t.val;
    return out;
}

bool operator ==(token l, token r){
    return l.val == r.val;
}

bool operator ==(token l, const char* r){
    token w = {r};
    return l==w;
}

bool operator !=(token l, token r){
    return !(l==r);
}

bool operator !=(token l, const char* r){
    return !(l!=token(r));
}


token& operator+(token&t, material f){
    t.mat = t.mat + f;
    return t;
}

token operator+(char* s, material m){
    return token(s, m);
}


token operator+(string s, material m){
    return token(s, m);
}






