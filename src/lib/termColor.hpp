#pragma once

#include <string>
using namespace std;



    // Represent standard 16 terminal colors 
enum class termColor{
    normal = 0,
    black = 90, red, green, yellow, blue, purple, cyan, white,
    dblack = 30, dred, dgreen, dyellow, dblue, dpurple, dcyan, dwihte 
};



    // Returns string that changes color of following glyphs displayed in terminal
string toString(termColor color){
    return "\033[" + to_string((int)color) + "m";
}
