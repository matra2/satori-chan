
# SatoriTui

This is a satori terminal client. it is faster and more convenient than the browser counterpart. 



## To download:
```
git clone https://gitlab.com/matra2/satoritui
```


## To install:

```
make install
```


## To run:
```
satorinal
```
