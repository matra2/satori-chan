
#include "src/defines/defines.hpp"
#include "src/html/parser.cpp"
#include "src/view/view.cpp"
#include <fstream>
#include <string>
using namespace std;




int main(){
    

    terminal("mkdir -p \"" + DEBUG_DIR + "\"");
    terminal("mkdir -p \"" + RUNTIME_DIR + "\"");
    terminal("mkdir -p \"" + TEMPLATES_DIR + "\"");
    terminal("mkdir -p \"" + CACHE_DIR + "\"");
    terminal("mkdir -p \"" + RESULTS_DIR + "\"");



    // scrape("https://satori.tcs.uj.edu.pl/contest/8674507/results");
    // return 0;

    // string link = "/contest/8244278/problems"; //PO prob
    // string link = "/contest/8244278/problems/8251524"; //wielomian
    // string link = "/contest/8237885/problems/8259440"; 
    std::string urlLink = "/contest/8237885/problems/8439021"; 
    urlLink = "https://satori.tcs.uj.edu.pl" + urlLink; 

    urlLink = "https://satori.tcs.uj.edu.pl/contest/8243058/problems/8348309";
    view v;
    // v.openPage(urlLink);
    v.openPageLocal(TEMPLATES_DIR + "index.html");
    // cout<<ROOTDIR + "/files/index.html"<<endl;
    while(v.handleInput());
    return 0;

}
