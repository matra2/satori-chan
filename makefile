
binName = satorinal
configDir = src/defines/rootDir.h

.DEFAULT_GOAL = build 



build: _setConfig
	@echo Compiling...
	g++ satorinal.cpp -o $(binName)
	@echo Done.

install: build _install

test-install: _setConfig _test-build _install

clear:
	@rm $(configDir)
	@rm $(binName)
	@rm -r logs
	@rm -r files/cache
	@rm -r files/results
	@rm -r files/runtime



_setConfig:
	@touch $(configDir)
	@echo "string ROOT_DIR = \"$(PWD)/\"; " > $(configDir)

_install:
	@echo Installing...
	@sudo cp $(binName) /usr/local/bin
	@echo Done.

_test-build: _setConfig
	@echo Compiling...
	g++ satorinal.cpp -o $(binName) -fmax-errors=5 #-fsanitize=addres
	@echo Done.




#INC = -Isrc/html -Isrc/lib -Isrc/defines -Isrc/printer

	
